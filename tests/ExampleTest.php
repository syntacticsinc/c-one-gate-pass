<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testJWTAuth()
    {
        $credentials = array(
            'username' => 'admin',
            'password' => 'admin',
            );
        $jwtAuth = new \App\GatePass\Auth\JWTAuthenticator();
        // dd($jwtAuth->apiLogin($credentials));
    }

    public function testRequestCreation(){
        $requestData = [
            "name" => "normal normal",
            "purpose" => "Test Purpose",
            "departure" => [
                "date" => "2016-12-04T05:56:52.051Z",
                "time" => "2016-12-04T05:00:54.086Z",
            ],
            "return" => [
                "date" => "2016-12-04T05:56:53.452Z",
                "time" => "2016-12-04T07:00:56.783Z",
            ],
                "hasVehicle" => true,
                "vehicle" => [
                    "id" => 1,
                    "number" => "31241",
                    "plate_number" => "ETA 546",
            ],
            "hasItems" => true,
            "items" => [
                0 => [
                    "name" => "Item 1",
                    "quantity" => 9,
                ],
                1 => [
                    "name" => "Item 2",
                    "quantity" => 15,
                ],
                2 => [
                    "name" => "Item 3",
                    "quantity" => 1,
                ],
            ],
            "user" => [
                "id" => 3,
                "username" => "normal",
                "first_name" => "normal",
                "last_name" => "normal",
                "email" => "normal@email.com",
                "role" => "normal",
            ],
            "sender" => [
                "id" => 3,
                "username" => "approver",
                "first_name" => "Approver",
                "last_name" => "Approver",
                "email" => "approver@email.com",
                "role" => "normal",
            ]
        ];

        $gatepass = $this->app->make('App\GatePass\GatePass');
        $gatepass->makeRequest($requestData);
        // dd($gatepass->makeRequest($requestData));
    }

    public function markAsRead(){
        // $user->gatePassRequests()
        //     ->with('notifications')
        //     ->get()
        //     ->get(4)
        //     ->notifications()
        //     ->updateExistingPivot($notification->id, ['read_status' => 1]);                                               
    }
}
