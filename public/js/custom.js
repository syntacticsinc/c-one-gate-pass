(function($){
    $(function(){
        $('ul.nav li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });

        $(".datepicker, #datepicker_departure, #datepicker_departure1, #datepicker_return, #datepicker_requested, #datepicker_requested1").datepicker();
        
//----------------------------------------------------------------------------------------
        $("#notification-options").hide();
        $("#notification-info").hide();
        $(".get-options").click(function() {
            $("#notification-options").show(500);
            $("#notification-list").slideUp(500, function() {

            });
        });
        $(".close-options").click(function() {
            $("#notification-options").hide(500);
            $("#notification-list").slideDown(500, function() {

            });
        });
        $(".get-info").click(function() {
            $("#notification-info").show(500);
            $("#notification-options").slideUp(500, function() {

            });
        });
        $(".back-list").click(function() {
            $("#notification-options").hide(500);
            $("#notification-info").hide(500);
            $("#notification-list").slideDown(500, function() {

            });
        });
        
//----------------------------------------------------------------------------------------
        $("#list-options").hide();
        $("#list-info").hide();
        $(".get-options").click(function() {
            $("#list-options").show(500);
            $("#user-list").slideUp(500, function() {

            });
        });
        $(".close-options").click(function() {
            $("#list-options").hide(500);
            $("#user-list").slideDown(500, function() {

            });
        });
        $(".get-info").click(function() {
            $("#list-info").show(500);
            $("#list-options").slideUp(500, function() {

            });
        });
        $(".back-list").click(function() {
            $("#list-options").hide(500);
            $("#list-info").hide(500);
            $("#user-list").slideDown(500, function() {

            });
        });
    });
})(window.jQuery);