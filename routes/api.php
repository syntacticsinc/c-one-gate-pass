<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Auth'], function(){
	Route::post('/login', 'LoginController@login');
	Route::post('/change-password', 'LoginController@changePassword');
	Route::get('/me', 'LoginController@getUser');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){

	Route::group(['prefix' => 'users'], function(){
		Route::get('/get-field-options', 'UsersController@getFieldOptions');
		Route::post('/store', 'UsersController@store');
		Route::get('/for-table', 'UsersController@usersForTable');
		Route::get('/search/{keyword}', 'UsersController@search');
		Route::post('/assign', 'UsersController@assign');
		Route::get('/{username}/assigned-users', 'UsersController@getAssignedUsers');
		Route::get('/{username}/details', 'UsersController@getUserByUsername');
		Route::get('/{username}/get-data', 'UsersController@getUserDataByUsername');
		Route::post('/{username}/update', 'UsersController@update');
		Route::post('/delete', 'UsersController@deleteUsers');
		Route::post('/reset', 'UsersController@resetUsers');
	});
	Route::group(['prefix' => 'settings'], function(){
		Route::post('/', 'SettingsController@store');
		Route::get('/', 'SettingsController@getSettings');
	});
	Route::group(['prefix' => 'vehicles'], function(){
		Route::post('/', 'VehiclesController@store');
		Route::get('/', 'VehiclesController@getVehicles');
		Route::get('/{id}/details', 'VehiclesController@getVehicle');
		Route::get('/{id}/delete', 'VehiclesController@delete');
	});	
	Route::group(['prefix' => 'dashboard'], function(){
		Route::get('/', 'DashboardController@getAllTotalRequests');
	});
	Route::group(['prefix' => 'requests'], function(){
		Route::get('/', 'RequestsController@get');
	});
});
Route::group(['prefix' => 'requests-monitoring', 'namespace' => 'Admin'], function(){
	Route::get('/', 'RequestMonitoringController@getrequests');
	Route::get('/actual-records', 'RequestMonitoringController@getActualRecords');
	Route::get('/departured/{request_id}', 'RequestMonitoringController@activateDepartured');
	Route::get('/returned/{request_id}', 'RequestMonitoringController@activateReturned');
	});


Route::get('/vehicles/search', 'Admin\VehiclesController@search');

Route::group(['prefix' => 'requests'], function(){
	Route::post('create', 'RequestsController@store');
});

Route::get('/requests/get-default-time', 'RequestsController@getDefaultTime');
Route::get('/requests/validate-vehicle-usage', 'RequestsController@validateVehicleUsage');
Route::get('/{username}/notifications', 'RequestsController@getNotificationsFor');
Route::get('/{username}/notifications/clear', 'RequestsController@clearNotifications');
Route::get('/{username}/notifications/{notificationId}/remove', 'RequestsController@removeNotificationOf');

Route::get('/notifications/{id}/get-info', 'RequestsController@getRequestInfo');

Route::post('/notifications/{id}/endorse', 'RequestsController@endorse');
Route::post('/notifications/{id}/approve', 'RequestsController@approve');
Route::post('/notifications/{id}/decline', 'RequestsController@decline');

Route::get('/{id}/requests/for-table', 'RequestsController@getAllRequests');
Route::get('/{id}/{type}/requests/for-table', 'RequestsController@getTypedRequests');

Route::post('/endorser/{endorser}/bulk-endorse', 'RequestsController@bulkEndorse');
Route::post('/approver/{approver}/bulk-approve', 'RequestsController@bulkApprove');
Route::post('/decline/{decliner}/bulk-decline', 'RequestsController@bulkDecline');
