const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass([
    	'./node_modules/nprogress/nprogress.css',
    	'./public/css/animate.css',
    	'./node_modules/toastr/toastr.scss',
    	'./node_modules/element-ui/lib/theme-default/index.css',
    	// './node_modules/element-ui/lib/theme-default/checkbox.css',
    	// './node_modules/element-ui/lib/theme-default/form-item.css',
    	// './node_modules/element-ui/lib/theme-default/message-box.css',
    	// './node_modules/element-ui/lib/theme-default/rate.css',
    	// './node_modules/element-ui/lib/theme-default/table.css',
    	// './node_modules/element-ui/lib/theme-default/autocomplete.css',
    	// './node_modules/element-ui/lib/theme-default/checkbox-group.css',
    	// './node_modules/element-ui/lib/theme-default/icon.css',
    	// './node_modules/element-ui/lib/theme-default/notification.css',
    	// './node_modules/element-ui/lib/theme-default/row.css',
    	// './node_modules/element-ui/lib/theme-default/table-column.css',
    	// './node_modules/element-ui/lib/theme-default/badge.css',
    	// './node_modules/element-ui/lib/theme-default/option.css',
    	// './node_modules/element-ui/lib/theme-default/select.css',
    	// './node_modules/element-ui/lib/theme-default/tab-pane.css',
    	// './node_modules/element-ui/lib/theme-default/base.css',
    	// './node_modules/element-ui/lib/theme-default/date-picker.css',
    	// './node_modules/element-ui/lib/theme-default/input.css',
    	// './node_modules/element-ui/lib/theme-default/option-group.css',
    	// './node_modules/element-ui/lib/theme-default/select-dropdown.css',
    	// './node_modules/element-ui/lib/theme-default/tabs.css',
    	// './node_modules/element-ui/lib/theme-default/breadcrumb.css',
    	// './node_modules/element-ui/lib/theme-default/dialog.css',
    	// './node_modules/element-ui/lib/theme-default/input-number.css',
    	// './node_modules/element-ui/lib/theme-default/pagination.css',
    	// './node_modules/element-ui/lib/theme-default/slider.css',
    	// './node_modules/element-ui/lib/theme-default/tag.css',
    	// './node_modules/element-ui/lib/theme-default/breadcrumb-item.css',
    	// './node_modules/element-ui/lib/theme-default/dropdown.css',
    	// './node_modules/element-ui/lib/theme-default/loading.css',
    	// './node_modules/element-ui/lib/theme-default/popover.css',
    	// './node_modules/element-ui/lib/theme-default/spinner.css',
    	// './node_modules/element-ui/lib/theme-default/time-picker.css',
    	// './node_modules/element-ui/lib/theme-default/button.css',
    	// './node_modules/element-ui/lib/theme-default/dropdown-item.css',
    	// './node_modules/element-ui/lib/theme-default/menu.css',
    	// './node_modules/element-ui/lib/theme-default/progress.css',
    	// './node_modules/element-ui/lib/theme-default/step.css',
    	// './node_modules/element-ui/lib/theme-default/time-select.css',
    	// './node_modules/element-ui/lib/theme-default/button-group.css',
    	// './node_modules/element-ui/lib/theme-default/dropdown-menu.css',
    	// './node_modules/element-ui/lib/theme-default/menu-item.css',
    	// './node_modules/element-ui/lib/theme-default/radio.css',
    	// './node_modules/element-ui/lib/theme-default/steps.css',
    	// './node_modules/element-ui/lib/theme-default/tooltip.css',
    	// './node_modules/element-ui/lib/theme-default/card.css',
    	// './node_modules/element-ui/lib/theme-default/menu-item-group.css',
    	// './node_modules/element-ui/lib/theme-default/radio-button.css',
    	// './node_modules/element-ui/lib/theme-default/submenu.css',
    	// './node_modules/element-ui/lib/theme-default/tree.css',
    	// './node_modules/element-ui/lib/theme-default/cascader.css',
    	// './node_modules/element-ui/lib/theme-default/form.css',
    	// './node_modules/element-ui/lib/theme-default/message.css',
    	// './node_modules/element-ui/lib/theme-default/radio-group.css',
    	// './node_modules/element-ui/lib/theme-default/switch.css',
    	// './node_modules/element-ui/lib/theme-default/upload.css',
    	'app.scss'
    	])
		.webpack('app.js');
	
});
