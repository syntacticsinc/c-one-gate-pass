<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleGatePassRequest extends Model
{
    protected $table = 'vehicle_requests';
    protected $fillable = [
    	'request_id',
    	'vehicle_id',
    ];
    public $timestamps = false;

    public function gatePassRequest(){
    	return $this->belongsTo('App\GatePassRequest');
    }

    public function vehicle(){
    	return $this->belongsTo('App\Vehicle');
    }
}
