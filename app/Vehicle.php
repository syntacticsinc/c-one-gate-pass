<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    //
    protected $fillable = [
        'name', 
        'number',
        'plate_number'
    ];

    public function requests(){
    	return $this->hasMany('App\VehicleGatePassRequest', 'request_id', 'id');
    }
}
