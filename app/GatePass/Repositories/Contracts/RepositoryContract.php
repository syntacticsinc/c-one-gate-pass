<?php 
namespace App\GatePass\Repositories\Contracts;

interface RepositoryContract {
	public function all($cols = array('*'));
}