<?php 
namespace App\GatePass\Repositories;
use App\GatePass\Repositories\AbstractRepository;
use Validator;
use App\Vehicle;

class VehicleRepository extends AbstractRepository {

	public function create($vehicledata = array()){
		if(empty($vehicledata)) return false;
		$validation = $this->validateNewVehicle($vehicledata);
		if($validation->fails()){
			return response()->json(['error' => $validation->errors()->all()], 401);
		}
		$result = $this->postVehicle($vehicledata);
		if(!$result['vehicle']){
			return response()->json(['error' => 'Unable to create vehicle.'], 401);
		}
		return response()->json(['message' => $result['message']], 200);
	}

	private function postVehicle($vehicledata){

		$result = array();
		$message = 'Successfully created a vehicle.';
        
        if(isset($vehicledata['id'])){
        	$vehicle = Vehicle::find($vehicledata['id']);
        	$message = 'Successfully updated the vehicle.';
        } else{
            $vehicle = new Vehicle;
        }
		$vehicle->name = $vehicledata['name'];
		$vehicle->description = $vehicledata['description'];
		$vehicle->plate_number = $vehicledata['plate_number'];
		$vehicle->save();

        
        $result = array(
        	'vehicle' => $vehicle,
        	'message' => $message
        );

		return $result;
	}

	private function validateNewVehicle($vehicledata){
		$rules = array(
			'name' => 'required|max:255',
			'description' => 'required|max:255',
			'plate_number' => 'required|max:255|unique:vehicles,plate_number',
		);
		if(isset($vehicledata['id']) && $vehicledata['id']) { 
			$rules['plate_number'] = $rules['plate_number'] . ',' . $vehicledata['id'] . ',id'; 
		}

		return Validator::make($vehicledata, $rules);
	}

	public function getVehicles($request){

	    $table = array();
		$search = $request->search ? $request->search : '';


        $vehicles = Vehicle::selectRaw('id, name, description, plate_number')
	        				->where(function($query) use ($search){
								$query->where('name', 'LIKE', '%' . $search . '%')
								        ->orWhere('description', 'LIKE', '%' . $search . '%')
										->orWhere('plate_number', 'LIKE', '%' . $search . '%');
							});
        $table['data'] = $vehicles->offset(($request->size * $request->currentPage) - $request->size)
								   ->limit($request->size)
								   ->get();


	    $table['data'] = $table['data']->sortBy('name')->toArray();
        $table['total'] = $search ? $vehicles->count() : $this->model->count(array('id'));


		return $table;
	}

	public function getVehicle($id){

		$vehicle = Vehicle::selectRaw('id, name, description, plate_number')
							->where('id', $id)
							->first();
		return $vehicle->toArray();
	}

	public function delete($id){

		$vehicle = Vehicle::find($id);
		$vehicle->delete();

		return "Success";
	}

	public function search($keyword){

        $vehicles = Vehicle::selectRaw('id, name, description, plate_number')
	        				->where('name', 'LIKE', '%' . $keyword . '%')
	        				->orWhere('description', 'LIKE', '%' . $keyword . '%')
	        				->orWhere('plate_number', 'LIKE', '%' . $keyword . '%')
	        				->get();
		return $vehicles;
	}

	protected function model(){
		return 'App\Vehicle';
	}

}