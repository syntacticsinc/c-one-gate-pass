<?php 
namespace App\GatePass\Repositories;
use App\GatePass\Repositories\AbstractRepository;
use Validator;
use App\User;
use App\Role;
use App\Endorser;
use App\Approver;
use Hash;
use Auth;
use Illuminate\Validation\Rule;

class UserRepository extends AbstractRepository {

	/**
	 * Create new user.
	 * @param  array  $userdata 
	 * @return Response           
	 */
	public function create($userdata = array()){
		if(empty($userdata)) return false;
		$validation = $this->validateNewUser($userdata);
		if($validation->fails()){
			return response()->json(['error' => $validation->errors()->all()], 401);
		}
		$user = $this->createUser($userdata);
		$user->endorsers()->sync($userdata['endorsers']);
		$user->approvers()->sync($userdata['approvers']);

		if(!$user){
			return response()->json(['error' => 'Unable to create user.'], 401);
		}
		return response()->json(['message' => 'Successfully created a user.'], 200);
	}

	/**
	 * Create new user object.
	 * @param  array $userdata 
	 * @return App\User           
	 */
	private function createUser($userdata){
		$selectedRole = Role::where('name', $userdata['role'])->first();

		$this->model->username = $userdata['username'];
		$this->model->first_name = $userdata['first_name'];
		$this->model->last_name = $userdata['last_name'];
		$this->model->email = $userdata['email'];
		$this->model->password = bcrypt($userdata['password']);
		$this->model->department = $userdata['department'];
		$this->model->save();

		$this->model->attachRole($selectedRole);
		return $this->model;
	}

	public function update($username, $userdata){
		if(empty($userdata)) return false;
		$validation = $this->validateUpdateUser($userdata);
		if($validation->fails()){
			return response()->json(['error' => $validation->errors()->all()], 401);
		}

		$user = $this->updateUser($username, $userdata);
		if(!$user){
			return response()->json(['error' => 'Unable to update user.'], 401);
		}
		return response()->json(['message' => 'Successfully updated user.'], 200);
	}

	private function updateUser($username, $userdata){
		$selectedRole = Role::where('name', $userdata['role'])->first();

		$user = $this->model->where('username', $username)->first();

		$user->username = $userdata['username'];
		$user->first_name = $userdata['first_name'];
		$user->last_name = $userdata['last_name'];
		$user->email = $userdata['email'];
		$user->department = $userdata['department'];

		if(isset($userdata['password']) && trim($userdata['password'])){
			$user->password = bcrypt($userdata['password']);
		}

		$user->save();

		$user->detachRoles();
		$user->attachRole($selectedRole);
		return $user;
	}

	private function validateNewUser($userdata){
		$rules = array(
				'username' => 'required|unique:users',
				'first_name' => 'required|min:2|max:255',
				'last_name' => 'required|min:2|max:255',
				'email' => 'required|unique:users|email',
				'role' => 'required',
				'department' => 'required',
				'password' => 'required|confirmed',
				'password_confirmation' => 'required',
				'approvers' => 'required_with:endorsers',
			);

		if(!count($userdata['approvers'])){
			unset($userdata['approvers']);
		}

		$messages = [
			'approvers.required_if' => 'An Approver is required.',
			'approvers.required_with' => 'An Approver is required if an Endorser is present.'
		];

		return Validator::make($userdata, $rules, $messages);
	}

	public function validateUpdateUser($userdata){
		$rules = array(
				'username' => [
					'required',
					Rule::unique('users')->ignore($userdata['id']),
				],
				'first_name' => 'required',
				'last_name' => 'required',
				'email' => [
					'required',
					'email',
					Rule::unique('users')->ignore($userdata['id']),
				],
				'role' => 'required',
				'department' => 'required',
				'password' => 'confirmed',
				'password_confirmation' => 'required_with:password',
			);
		return Validator::make($userdata, $rules);
	}

	public function getUserByUsername($username){
		$user = $this->model->selectRaw(
					'users.id, 
					users.username, 
					users.first_name,
					users.last_name,
					users.email, 
					users.department, 
					roles.display_name as role')
				->join('role_user', 'role_user.user_id', '=', 'users.id')
				->join('roles', 'role_user.role_id', '=', 'roles.id')
				->where('users.username', $username)
				->first();
		$data = $user->toArray();		
		$data['endorsers'] = $user->endorsers->toArray();
		$data['approvers'] = $user->approvers->toArray();
		return $data;
	}

	/**
	 * Get all users to display to a table.
	 * @param  \Illuminate\Http\Request $request 
	 * @return \Illuminate\Http\Response          
	 */
	public function getUsersForTable($request){
		$table = array();
		$search = $request->search ? $request->search : '';
		$role = $request->options['role'];
		$users = $this->model->selectRaw(
					'users.id, 
					users.username, 
					CONCAT(users.first_name, " ", users.last_name) as name,
					users.email, 
					users.department, 
					roles.display_name as role')
				->join('role_user', 'role_user.user_id', '=', 'users.id')
				->join('roles', 'role_user.role_id', '=', 'roles.id')
				->where(function($query) use ($search){
					$query->where('users.username', 'LIKE', '%' . $search . '%')
							->orWhere('users.first_name', 'LIKE', '%' . $search . '%')
							->orWhere('users.last_name', 'LIKE', '%' . $search . '%')
							->orWhere('users.email', 'LIKE', '%' . $search . '%')
							->orWhere('users.department', 'LIKE', '%' . $search . '%')
							->orWhere('roles.display_name', 'LIKE', '%' . $search . '%');
				})
				->whereNotIn('users.id', $request->exclude);

		if($role){
			$users = $users->where('roles.name', $role);
		}

		$table['data'] = $users->offset(($request->size * $request->currentPage) - $request->size)
								->orderBy('users.created_at')
								->limit($request->size)
								->get();
		foreach($table['data'] as $user){
			$user['endorsers'] = $user->endorsers;
			$user['approvers'] = $user->approvers;
		}
		$table['total'] = $search || $role ? 
								$table['data']->count() 
								: $this->model
								->with('roles')
								->whereNotIn('id', $request->exclude)
								->count(array('id'));

		$table['user_count'] = $this->model->selectRaw('id')
									->join('role_user', 'role_user.user_id', '=', 'users.id')
									->join('roles', 'role_user.role_id', '=', 'roles.id')
									->whereNotIn('users.id', $request->exclude)
									->count(array('users.id'));

		$table['admin_count'] = $this->model->selectRaw('id')
									->join('role_user', 'role_user.user_id', '=', 'users.id')
									->join('roles', 'role_user.role_id', '=', 'roles.id')
									->where('roles.name', 'admin')
									->whereNotIn('users.id', $request->exclude)
									->count(array('users.id'));
		return response()->json($table);
	}

	/**
	 * Search users whose ids are not in $selectedUsers
	 * @param  string $keyword       
	 * @param  array $selectedUsers 
	 * @return array                
	 */
	public function searchNotIn($keyword, $selectedUsers, $username){
		$selectedUsers = $selectedUsers ? $selectedUsers : array();
		// 1
		// $endorsedUsers = Endorser::where('username', $username)
		// 							->first()
		// 							->employees()
		// 							->select('users.id')
		// 							->get()
		// 							->map(function($user){ 
		// 								return $user->id; 
		// 							})
		// 							->toArray();
		// 2
		// $approvedUsers = Approver::where('username', $username)
		// 							->first()
		// 							->employees()
		// 							->select('users.id')
		// 							->get()
		// 							->map(function($user){ 
		// 								return $user->id; 
		// 							})
		// 							->toArray();
		$searched = $this->model
						->where(function($query) use ($keyword) {
							$query->where('username', 'LIKE', '%' . $keyword . '%')
								->orWhere('first_name', 'LIKE', '%' . $keyword . '%')
								->orWhere('last_name', 'LIKE', '%' . $keyword . '%');
						})
						->take(10)
						->get();
		// 3
		// $selectedUsers = array_merge($selectedUsers, $endorsedUsers, $approvedUsers);
		$searched = $searched->filter(function($value, $key) use ($selectedUsers) {
			return !in_array($value->id, $selectedUsers);
		})->filter(function($value) {
			return !$value->hasRole('admin') && !$value->hasRole('monitoring');
		})->filter(function($value) use ($username) {
			return $value->username != $username;
		});
		return $searched->toArray();
	}

	public function search($keyword){
		$searched = $this->model
						->search($keyword)
						->take(10)
						->get();

		return $searched->toArray();
	}

	public function assign($assignments){
		$user = $this->model->where('username', $assignments['username'])->first();
		if(!$user){
			return response()->json(['error' => 'Unable to find user with a username ' . $assignments['username']], 401);
		}

		$validation = $this->validateAssignments($assignments);
		if($validation->fails()){
			return response()->json(['error' => $validation->errors()->all()], 401);
		}

		$user->endorsers()->sync($assignments['endorsers']);
		$user->approvers()->sync($assignments['approvers']);
		// return response()->json(['message' => 'Successfully assigned users to ' . $user->full_name . '.'], 200);
		return response()->json(['message' => 'Operation Completed'], 200);
	}

	/**
	 * Validate assignments.
	 * @param  array $assignments 
	 * @return Validator              
	 */
	public function validateAssignments($assignments){
		$rules = [
			'approvers' => 'required|required_with:endorsers',
		];

		if(!count($assignments['approvers'])){
			unset($assignments['approvers']);
		}

		$messages = [
			'approvers.required' => 'An Approver is required.',
			'approvers.required_with' => 'An Approver is required if an Endorser is present.'
		];

		return Validator::make($assignments, $rules, $messages);
	}

	/**
	 * Get endorsers and approvers of a user by its username.
	 * @param  string $username 
	 * @return array           
	 */
	public function getAssignedUsersFor($username){
		if(!$username) return response()->json(['error' => 'Invalid user.'], 401);

		$user = $this->model
					->select('id', 'username', 'first_name', 'last_name', 'email', 'department')
					->where('username', $username)
					->first();
		return array(
				'user' => $user,
				'endorsers' => $user->endorsers->toArray(),
				'approvers' => $user->approvers->toArray(),
			);
	}

	public function getUserDataByUsername($username){
		$user = $this->model->selectRaw(
					'users.id, 
					users.username, 
					users.first_name,
					users.last_name,
					users.email, 
					users.department, 
					roles.name as role')
				->join('role_user', 'role_user.user_id', '=', 'users.id')
				->join('roles', 'role_user.role_id', '=', 'roles.id')
				->where('users.username', $username)
				->first();
		return $user->toArray();
	}

	public function deleteUsers($userIds){
		$this->model
			->destroy($userIds);
		return response()->json(['message' => 'User(s) has been successfully deleted!'], 200);
	}

	public function resetUsers($userIds){
		$this->model
				->whereIn('id', $userIds)
				->update(['password' => Hash::make(config('cone_settings.default_password'))]);
		return response()->json(['message' => 'User(s) has been reseted successfully!'], 200);
	}

	/**
	 * Get users that has user with username "username" as their endorser or approver.
	 * @param  string $username 
	 * @param  string $keyword  
	 * @return array           
	 */
	public function getFollowersOf($username, $keyword){
		$users = array();
		foreach(['\App\Endorser', '\App\Approver'] as $userType){
			$users = array_merge($users, $userType::where('users.username', $username)
										->first()
										->employees()
										->where(function($query) use ($keyword){
											$query->where('users.username', 'LIKE', '%' . $keyword . '%')
													->orWhere('users.first_name', 'LIKE', '%' . $keyword . '%')
													->orWhere('users.last_name', 'LIKE', '%' . $keyword . '%')
													->orWhere('users.username', 'LIKE', '%' . $keyword . '%')
													->orWhere('users.email', 'LIKE', '%' . $keyword . '%');
										})
										->limit(10)
										->get([
											'users.id', 
											'users.username', 
											'users.first_name', 
											'users.last_name', 
											'users.email'
										])
										->toArray());
		}
		return $users;
	}

	public function getAllUsers($keyword){
		$users = $this->model
					->selectRaw('users.id, users.username, users.first_name, users.last_name, users.email')
					->join('role_user', 'users.id', '=', 'role_user.user_id')
					->join('roles', 'role_user.role_id', '=', 'roles.id')
					->where(function($query) use ($keyword) {
						$query->where('users.username', 'LIKE', '%' . $keyword . '%')
								->orWhere('users.first_name', 'LIKE', '%' . $keyword . '%')
								->orWhere('users.last_name', 'LIKE', '%' . $keyword . '%')
								->orWhere('users.username', 'LIKE', '%' . $keyword . '%')
								->orWhere('users.email', 'LIKE', '%' . $keyword . '%');
					})
					->where('roles.name', 'normal')
					->limit(10)
					->get()   
					->toArray();
		return $users;
	}

	protected function model(){
		return 'App\User';
	}
}