<?php 
namespace App\GatePass\Repositories;
use App\GatePass\Repositories\Contracts\RepositoryContract;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\VehicleGatePassRequest;
use App\ItemGatePassRequest;
use App\User;

class RequestsRepository extends AbstractRepository {

	public function create($request){
		$this->model->user_id = $request['user']['id'];
		$this->model->departured_at = Carbon::parse($request['departure']['date'])->tz('Asia/Manila')->format('Y-m-d') . ' ' . Carbon::parse($request['departure']['time'])->tz('Asia/Manila')->format('H:i:s');
		$this->model->returned_at = Carbon::parse($request['return']['date'])->tz('Asia/Manila')->format('Y-m-d') . ' ' . Carbon::parse($request['return']['time'])->tz('Asia/Manila')->format('H:i:s');
		$this->model->guest = $request['name'];	
		$this->model->destination = $request['destination'];
		$this->model->purpose = $request['purpose'];
		$this->model->status = $request['status'];	
		$this->model->monitoring_status = 'pending';
		$this->model->endorsed_by = isset($request['endorsed_by']) ? $request['endorsed_by'] : null;
		$this->model->approved_by = isset($request['approved_by']) ? $request['approved_by'] : null;

		$this->model->save();

		if($request['hasVehicle']){
			$this->attachVehicle($request['vehicle']);
		}

		if($request['hasItems']){
			$this->attachItems($request['items']);
		}

		return $this->model;
	}

	private function attachVehicle($vehicle){
		$vehicleRequest = new VehicleGatePassRequest;
		$vehicleRequest->request_id = $this->model->id;
		$vehicleRequest->vehicle_id = $vehicle['id'];
		$vehicleRequest->save();
		return $this->model->save();
	}

	private function attachItems($items){
		$items = collect($items)->map(function($item){
			return (new ItemGatePassRequest($item));
		});
		return $this->model->items()->saveMany($items);
	}

	public function getNotificationsFor($username, $request){
		$mode = $request->mode;
		$repo = $this;
		$user = User::where('username', $username)
					->first();

		$notifications = $mode == 'unread' ? $user->notifications()
								->wherePivot('read_status', 0)
								->orderBy('created_at', 'desc')
								->limit(8)
								->get() : 
								$user->notifications()
									->orderBy('created_at', 'desc')
									->orderBy('notification_user.read_status')
									->limit($request->step * 5)
									->get();
		$notifications = $notifications->map(function($notification) use ($repo, $user){
			$tmpNotification = array(
				'id' => $notification->id,
				'status' => $notification->status
				);
			$tmpNotification = array_merge($tmpNotification, $repo->buildTextFor($notification, $user));
			return $tmpNotification;
		});
		return $notifications;
	}

	private function buildTextFor($notification, $sender){
		$owner = $notification->user;
		$isOwner = $owner == $sender;
		$notificationText = '';
		$requestType = 'Itinerary Gate Pass';
		if($notification->vehicle instanceof VehicleGatePassRequest) $requestType = 'Vehicle Gate Pass';
		if($notification->items->count()) $requestType = 'Item Gate Pass';
		if($notification->vehicle instanceof VehicleGatePassRequest && $notification->items->count()){
			$requestType = 'Vehicle & Item Gate Pass';
		}
		switch($notification->status){
			case 'endorsement': 
				$notification = $this->makeEndorsementTextFor($notification, $requestType, $sender, $isOwner, $owner);
				break;
			case 'approval':
				$notification = $this->makeApprovalTextFor($notification, $requestType, $sender, $isOwner, $owner);
				break;
			case 'approved':
				$notification = $this->makeApprovedTextFor($notification, $requestType, $sender, $isOwner, $owner);
				break;
			case 'declined':
				$notification = $this->makeDeclinedTextFor($notification, $requestType, $sender, $isOwner, $owner);
				break;
			case 'expired':
				$notification = $this->makeExpiredTextFor($notification, $requestType, $sender, $isOwner, $owner);
				break;
		}
		return $notification;
	}

	private function makeEndorsementTextFor($notification, $requestType, $sender, $isOwner, $owner){
		$defaultText = $isOwner ? 
				$requestType . ' on ' . $notification->departured_at->tz('Asia/Manila')->format('m/d/Y') . ' has been sent.' : 
				$owner->fullname . ' ' . $requestType . ' Request.';
		$tmp = array(
			'text' => $defaultText,
			'type' => 'pending',
			'clickable' => !$isOwner,
			);

		return $tmp;
	}

	private function makeApprovalTextFor($notification, $requestType, $sender, $isOwner, $owner){
		$defaultText = $isOwner ? $requestType . ' is now up for Approval.' : $notification->user->fullname . ' ' . $requestType . ' is now up for Approval.';

		$tmp = array(
			'text' => $defaultText,
			'type' => 'approval',
			'clickable' => !$isOwner,
			);

		return $tmp;
	}

	private function makeApprovedTextFor($notification, $requestType, $sender, $isOwner, $owner){
		$defaultText = $owner ? $requestType . ' is approved by ' . $notification->approver->fullname . ($notification->expiration ? ' until ' . $notification->expiration->tz('Asia/Manila')->format('m/d/Y h:i A') : '') : '';

		$tmp = array(
			'text' => $defaultText,
			'type' => 'approved',
			'clickable' => !$isOwner,
			);

		return $tmp;
	}

	private function makeDeclinedTextFor($notification, $requestType, $sender, $isOwner, $owner){
		$defaultText = $isOwner ? $requestType . ' has been declined.[' . ($notification->reason ? $notification->reason : 'No Reason.') . ']' : '';

		$tmp = array(
			'text' => $defaultText,
			'type' => 'pending',
			'clickable' => !$isOwner,
			);

		return $tmp;
	}

	private function makeExpiredTextFor($notification, $requestType, $sender, $isOwner, $owner){
		$defaultText = $isOwner ? $requestType . ' has expired.' : '';

		$tmp = array(
			'text' => $defaultText,
			'type' => 'pending',
			'clickable' => !$isOwner,
			);

		return $tmp;
	}

	protected function model(){
		return 'App\GatePassRequest';
	}
}