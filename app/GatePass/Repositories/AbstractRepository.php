<?php 
namespace App\GatePass\Repositories;
use App\GatePass\Repositories\Contracts\RepositoryContract;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractRepository implements RepositoryContract{
	protected $model;
	protected $app;

	public function __construct(App $app){
		$this->app = $app;
		$this->model = $this->makeModel();
	}

	protected function makeModel(){
		$model = $this->app->make($this->model());

		if(!$model instanceof Model){
			throw new \Exception($this->model() . ' is not an instance of Illuminate\Database\Eloquent\Model');
		}

		return $model;
	}

	public function all($cols = array('*')){
		return $this->model->all();
	}

	abstract protected function model();
}