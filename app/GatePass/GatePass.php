<?php 

namespace App\GatePass;
use App\GatePass\Repositories\RequestsRepository;
use Validator;
use App\User;
use App\Endorser;
use App\Approver;
use App\GatePassRequest;
use App\RequestNotification;
use App\VehicleGatePassRequest;
use Carbon\Carbon;
use DB;

class GatePass {

	/**
	 * Request Status/Notification Types:
	 *
	 * endorsement == pending
	 * approval == pending
	 * approved == ongoing
	 * returned == completed
	 * declined == declined
	 * expired == expired
	 */

	protected $requestRepository;

	public function __construct(RequestsRepository $requestRepository){
		$this->requestRepository = $requestRepository;
	}

	public function makeRequest($requestData){
		$validation = $this->validateRequestCreation($requestData);
		if($validation->fails()){
			return response()->json(['error' => $validation->errors()->all()], 500);
		}
        $requestData['name'] = !$requestData['user'] ? $requestData['name'] : null;
		$requestData['status'] = 'endorsement';

        $sender = User::find($requestData['sender']['id']);

        if(!$sender->endorsers->count()){
            $requestData['status'] = 'approval';
        }

		if($requestData['sender'] !== $requestData['user'] && $requestData['user']){
			$isEndorser = Endorser::findOrFail($requestData['sender']['id'])
									->employees()
									->where('users.id', $requestData['user']['id'])
									->count() > 0;
			// If the sender is the user's 'Endorser'
			// Then the request's status is for 'approval', because its already endorsed by the sender
			// Else if the sender is not the user's 'Endorser'
			// Then the sender is an 'Approver', thus the request's status is 'approved'.
			$requestData['status'] = $isEndorser ? 'approval' : 'approved';
			// If the sender is the user's 'Endorser'
			$requestData['endorsed_by'] = $isEndorser ? $requestData['sender']['id'] : null;
			// If the sender is not the user's 'Endorser'
			// Then the sender is an 'Approver'
			$requestData['approved_by'] = !$isEndorser ? $requestData['sender']['id'] : null;
		}

        if(!$requestData['user']){
            $requestData['user'] = $requestData['sender'];
            $requestData['approved_by'] = $sender->hasRole('admin') ? $sender->id : null;
            $requestData['status'] = $sender->hasRole('admin') ? 'approved' : $requestData['status'];
        }

		$request = $requestData['user'] ? $this->requestRepository
						->create($requestData) : $this->requestRepository->createRequestForGuest($requestData);

		if($requestData['status'] == 'endorsement'){
			$this->notifyEndorsersOf($request);
		}
		if($requestData['status'] == 'approval'){
			$this->notifyApproversOf($request);
		}
        if($requestData['status'] == 'approved' && $request->user){
            $request->user
                    ->notifications()
                    ->save($request, ['read_status' => false]);
        }

		return $request;
	}

	public function notifyEndorsersOf($request){
		$user = $request->user;
		$endorsers = $user->endorsers;
        $user->notifications()
                ->save($request, ['read_status' => false]);
		$endorsers->map(function($endorser) use ($request){
			$endorser->notifications()
					->save($request, ['read_status' => false]);
		});
	}

    public function notifyUser($request){
        $user = $request->user;
        $user->notifications()
                ->save($request, ['read_status' => false]);
    }

	public function notifyApproversOf($request){
		$user = $request->user;
		$approvers = $user->approvers;
        $user->notifications()
                ->save($request, ['read_status' => false]);
		$approvers->map(function($approver) use ($request){
			$approver->notifications()
					->save($request, ['read_status' => false]);
		});
	}

	public function notifyAllOf($request){
		$user = $request->user;

		$this->notifyEndorsersOf($request);
		$this->notifyApproversOf($request);
	}

    private function validateRequestCreation($request){
    	$rules = array(
    			'name' => 'required',
                'destination' => 'required',
    			'purpose' => 'required',
    			'departure.date' => 'required',
    			'departure.time' => 'required',
    			'return.date' => 'required',
    			'return.time' => 'required',
    			'vehicle' => 'required_if:hasVehicle,true',
                'vehicle.id' => 'required_if:hasVehicle,true',
    			'vehicle.name' => 'required_if:hasVehicle,true',
                // 'vehicle.plate_number' => 'required_if:vehicle.id,',
    			'items' => 'required_if:hasItems,true',
    			'items.*.name' => 'required_if:hasItems,true',
    			'items.*.quantity' => 'required_if:hasItems,true|integer|min:1',
    		);

    	$messages = array(
                'name.required' => 'The employee name is required.',
				'departure.date.required' => 'The departure date field is required.',
				'departure.time.required' => 'The departure time field is required.',
				'return.date.required' => 'The return date field is required.',
				'return.time.required' => 'The return time field is required.',
				'vehicle.name.required_if' => 'Vehicle is required.',
                // 'vehicle.plate_number.required_if' => 'The vehicle field is required.',
				'items.required' => 'Item(s) is required if item gate pass is checked.',
				'items.*.name.required_if' => 'The Item name is required.',
                'vehicle.id.required_if' => 'Vehicle must be an existing vehicle.',
				'items.*.quantity.min' => 'The minimum item quantity is (1).',
    		);

    	return Validator::make($request, $rules, $messages);
    }


    public function getNotificationsFor($username, $request){
    	return $this->requestRepository
    				->getNotificationsFor($username, $request);
    }

    public function clearNotificationsOf($notifications, $username){
    	$user = User::where('username', $username)->first();

    	DB::update('UPDATE `notification_user` SET `notification_user`.`read_status` = 1 WHERE `notification_user`.`request_id` IN (' . implode(', ', $notifications) . ') AND `notification_user`.`user_id` = ' . $user->id);

    	return response()->json(['message' => 'Notification(s) Cleared!'], 200);
    }

    public function getRequestInfo($id){
    	$request = GatePassRequest::findOrFail($id);
    	$vehicleRequest = $request->vehicle;
    	$requestInfo = [
    		'requested_by' => $request->user->fullname,
            'guest' => $request->guest,
    		'departured_date' => $request->departured_at->format('m/d/Y'),
    		'departured_time' => $request->departured_at->format('h:i A'),
    		'returned_date' => $request->returned_at->format('m/d/Y'),
    		'returned_time' => $request->returned_at->format('h:i A'),
            'has_expiration' => $request->expiration ? true : false,
            'expiration_date' => $request->expiration ? $request->expiration->format('Y-m-d') : null,
            'expiration_time' => $request->expiration ? $request->expiration->format('H:i') : null,
            'destination' => $request->destination,
    		'purpose' => $request->purpose,
            'reason' => $request->reason,
            'status' => $request->status,
    		'endorser' => ($request->endorser ? $request->endorser->fullname : ''),
            'approver' => ($request->approver ? $request->approver->fullname : ''),
            'actual_departured_at' => $request->actual_departured_at ? $request->actual_departured_at->format('m/d/Y h:i a') : $request->actual_departured_at,
            'actual_returned_at' => $request->actual_returned_at ? $request->actual_returned_at->format('m/d/Y h:i a') : $request->actual_returned_at,
    	];

    	if($vehicleRequest){
    		$vehicle = $vehicleRequest->vehicle;
    		$requestInfo['vehicle'] = [
    			'name' => $vehicle->name,
    			'description' => $vehicle->description,
    			'plate_number' => $vehicle->plate_number,
    		];
    	}

    	if($request->items->count()){
    		$requestInfo['items'] = $request->items;
    	}

    	return $requestInfo;
    }

    public function endorse($id, $request){
    	$validation = $this->validateRequestEndorsementApproval($request->all());
    	if($validation->fails()){
    		return response()->json(['error' => $validation->errors()->all()], 500);
    	}
    	$hasExpiration = $request->has_expiration == 'true' ? true : false;
    	$gatePassRequest = GatePassRequest::findOrFail($id);
        if($gatePassRequest->endorsed_by){
            $endorser = User::findOrFail($gatePassRequest->endorsed_by);
            return response()->json(['error' => ['This request was already endorsed by ' . $endorser->fullName]], 500);
        }
        if($gatePassRequest->declined_by){
            $decliner = User::findOrFail($gatePassRequest->declined_by);
            return response()->json(['error' => ['This request was already declined by ' . $decliner->fullName]], 500);
        }
    	$gatePassRequest->endorsed_by = $request->endorser;
    	$gatePassRequest->status = 'approval';
        $gatePassRequest->expiration = null;
    	if($hasExpiration){
    		$gatePassRequest->expiration = (Carbon::parse($request['expiration']['date'])->tz('Asia/Manila')->format('Y-m-d') . ' ' . Carbon::parse($request['expiration']['time'])->tz('Asia/Manila')->format('H:i:s'));
    	}
    	$gatePassRequest->save();

    	DB::update('DELETE FROM  `notification_user` WHERE `notification_user`.`request_id` = ' . $gatePassRequest->id);

    	$this->notifyApproversOf($gatePassRequest);

    	return response()->json(['message' => 'Endorsement Successfully Sent!']);
    }

    public function bulkEndorse($endorserId, $request){
        $ids = $request->all();
        $tmp = $this;
        $validation = $this->validateRequestEndorsementApproval($request->all());
        if($validation->fails()){
            return response()->json(['error' => $validation->errors()->all()], 500);
        }
        $gatePassRequests = GatePassRequest::whereIn('id', $ids)->whereNull('endorsed_by')->update([
            'endorsed_by' => $endorserId,
            'status' => 'approval',
            ]);

        DB::update('DELETE FROM `notification_user` WHERE `notification_user`.`request_id` IN (' . trim(implode(', ', $ids), ', ') . ')');

        $gatePassRequests = GatePassRequest::whereIn('id', $ids)->get();
        $gatePassRequests->map(function($request) use ($tmp) {
            $tmp->notifyApproversOf($request);
        });

        return response()->json(['message' => 'Endorsement Successfully Sent!']);
    }

    public function approve($id, $request){
    	$validation = $this->validateRequestEndorsementApproval($request->all());
    	if($validation->fails()){
    		return response()->json(['error' => $validation->errors()->all()], 500);
    	}
    	$hasExpiration = $request->has_expiration == 'true' ? true : false;
    	$gatePassRequest = GatePassRequest::findOrFail($id);
        if($gatePassRequest->approved_by){
            $approver = User::findOrFail($gatePassRequest->approved_by);
            return response()->json(['error' => ['This request was already approved by ' . $approver->fullName]], 500);
        }
        if($gatePassRequest->declined_by){
            $decliner = User::findOrFail($gatePassRequest->declined_by);
            return response()->json(['error' => ['This request was already declined by ' . $decliner->fullName]], 500);
        }
    	$gatePassRequest->approved_by = $request->approver;
    	$gatePassRequest->status = 'approved';
        $gatePassRequest->expiration = null;
    	if($hasExpiration){
    		$gatePassRequest->expiration = (Carbon::parse($request['expiration']['date'])->tz('Asia/Manila')->format('Y-m-d') . ' ' . Carbon::parse($request['expiration']['time'])->tz('Asia/Manila')->format('H:i:s'));
    	}
    	$gatePassRequest->save();

    	DB::update('DELETE FROM `notification_user` WHERE `notification_user`.`request_id` = ' . $gatePassRequest->id);

    	$this->notifyAllOf($gatePassRequest);

    	return response()->json(['message' => 'Approval Successfully Sent!']);
    }

    public function bulkApprove($approverId, $request){
        $ids = $request->all();
        $tmp = $this;
        $validation = $this->validateRequestEndorsementApproval($request->all());
        if($validation->fails()){
            return response()->json(['error' => $validation->errors()->all()], 500);
        }
        $gatePassRequests = GatePassRequest::whereIn('id', $ids)->whereNull('approved_by')->update([
            'approved_by' => $approverId,
            'status' => 'approved',
            ]);
        DB::update('DELETE FROM `notification_user` WHERE `notification_user`.`request_id` IN (' . trim(implode(', ', $ids), ', ') . ')');

        $gatePassRequests = GatePassRequest::whereIn('id', $ids)->get();
        $gatePassRequests->map(function($request) use ($tmp) {
            $tmp->notifyUser($request);
        });

        return response()->json(['message' => 'Approval Successfully Sent!']);
    }
    
    public function decline($id, $request){
    	$gatePassRequest = GatePassRequest::findOrFail($id);
        $decliner = User::findOrFail($request->decliner);
        if($gatePassRequest->declined_by){
            $user = User::findOrFail($gatePassRequest->declined_by);
            return response()->json(['error' => ['This request was already declined by ' . $user->fullName]], 500);
        }
        if($gatePassRequest->endorsed_by && !$decliner->hasRole('admin')){
            $endorser = User::findOrFail($gatePassRequest->endorsed_by);
            return response()->json(['error' => ['This request was already endorsed by ' . $endorser->fullName]], 500);
        }
        if($gatePassRequest->approved_by){
            $approver = User::findOrFail($gatePassRequest->approved_by);
            return response()->json(['error' => ['This request was already approved by ' . $approver->fullName]], 500);
        }
    	$gatePassRequest->declined_by = $request->decliner;
    	$gatePassRequest->status = 'declined';
    	$gatePassRequest->reason = $request->reason;
    	$gatePassRequest->save();

    	DB::update('DELETE FROM `notification_user` WHERE `notification_user`.`request_id` = ' . $gatePassRequest->id);

    	$this->notifyUser($gatePassRequest);

    	return response()->json(['message' => 'Request Declined!']);
    }
    
    public function bulkDecline($decliner, $request){
        $ids = $request->all();
        $tmp = $this;
        $gatePassRequests = GatePassRequest::whereIn('id', $ids)->update([
            'declined_by' => $decliner,
            'status' => 'declined',
            'reason' => $request->reason,
            ]);

        DB::update('DELETE FROM `notification_user` WHERE `notification_user`.`request_id` IN (' . trim(implode(', ', $ids), ', ') . ')');

        $gatePassRequests = GatePassRequest::whereIn('id', $ids)->get();
        $gatePassRequests->map(function($request) use ($tmp) {
            $tmp->notifyUser($request);
        });

        return response()->json(['message' => 'Request(s) Declined!']);
    }

    private function validateRequestEndorsementApproval($data){
    	$rules = [
    		'expiration.date' => 'required_if:has_expiration,true',
    		'expiration.time' => 'required_if:has_expiration,true',
    	];

    	$messages = [
    		'expiration.date.required_if' => 'Expiration date is required.',
    		'expiration.time.required_if' => 'Expiration time is required.'
    	];

    	return Validator::make($data, $rules, $messages);
    }

    public function getAllRequestsOf($id, $request){
        $gatePassRequests = GatePassRequest::selectRaw('requests.id, 
                            DATE_FORMAT(requests.created_at, "%c/%e/%Y") as date_requested,
                            DATE_FORMAT(requests.departured_at, "%c/%e/%Y") as departure_date,     
                            TIME(requests.departured_at) as departure_time,  
                            DATE(requests.returned_at) as return_date,
                            TIME(requests.returned_at) as return_time,
                            IFNULL(requests.guest, "N/A") as guest,
                            IFNULL(CONCAT(endorsers_list.first_name, " ", endorsers_list.last_name), "N/A") as endorser,
                            IFNULL(CONCAT(approvers_list.first_name, " ", approvers_list.last_name), "N/A") as approver,
                            CONCAT(UCASE(SUBSTRING(requests.status, 1, 1)),SUBSTRING(requests.status, 2)) as status,
                            users.first_name as first_name,
                            users.last_name as last_name,
                            CONCAT(users.first_name, " ", users.last_name) as employee_name')           
                    ->join('users', 'users.id', '=', 'requests.user_id')
                    ->leftJoin('users as endorsers_list', 'requests.endorsed_by', '=', 'endorsers_list.id')
                    ->leftJoin('users as approvers_list', 'requests.approved_by', '=', 'approvers_list.id')
                    ->where(function($query) use ($id) {
                        $query->where('requests.user_id', $id)
                            ->orWhere('requests.endorsed_by', $id)
                            ->orWhere('requests.approved_by', $id)
                            ->orWhere('requests.declined_by', $id);
                    });

        return $this->filterGatePassRequests($id, $gatePassRequests, $request);
    }

    public function getTypedRequests($id, $type, $request){
        $users = array_column(Endorser::findOrFail($id)
                        ->employees()
                        ->select(['users.id'])
                        ->get()
                        ->toArray(), 'id');
        if($type == 'approval'){
            $users = array_column(Approver::findOrFail($id)
                            ->employees()
                            ->select(['users.id'])
                            ->get()
                            ->toArray(), 'id');
        }

        $gatePassRequests = GatePassRequest::selectRaw('requests.id, 
                            DATE_FORMAT(requests.created_at, "%c/%e/%Y") as date_requested,
                            DATE_FORMAT(requests.departured_at, "%c/%e/%Y") as departure_date,     
                            TIME(requests.departured_at) as departure_time,  
                            DATE(requests.returned_at) as return_date,
                            TIME(requests.returned_at) as return_time,
                            IFNULL(requests.guest, "N/A") as guest,
                            CONCAT(endorsers_list.first_name, " ", endorsers_list.last_name) as endorser,
                            CONCAT(approvers_list.first_name, " ", approvers_list.last_name) as approver,
                            CONCAT(UCASE(SUBSTRING(requests.status, 1, 1)),SUBSTRING(requests.status, 2)) as status,
                            users.first_name as first_name,
                            users.last_name as last_name,
                            CONCAT(users.first_name, " ", users.last_name) as employee_name')           
                    ->join('users', 'users.id', '=', 'requests.user_id')
                    ->leftJoin('users as endorsers_list', 'requests.endorsed_by', '=', 'endorsers_list.id')
                    ->leftJoin('users as approvers_list', 'requests.approved_by', '=', 'approvers_list.id')
                    ->whereNotIn('requests.status', ['expired', 'declined'])
                    ->whereIn('requests.user_id', $users);

        if($type == 'endorsement'){
            $gatePassRequests = $gatePassRequests->whereNull('requests.endorsed_by')
                                                    ->whereNull('requests.approved_by')
                                                    ->where('status', 'endorsement');
        }

        if($type == 'approval'){
            // $gatePassRequests = $gatePassRequests->whereNotNull('requests.endorsed_by')
            //                                         ->whereNull('requests.approved_by');
            $gatePassRequests = $gatePassRequests->whereNull('requests.approved_by')
                                                ->where('status', 'approval');
        }

        return $this->filterGatePassRequests($id, $gatePassRequests, $request);
    }

    private function filterGatePassRequests($id, $gatePassRequests, $request){
        $keyword = isset($request->search['keyword']) && !empty(trim($request->search['keyword'])) ? $request->search['keyword'] : false;
        $dateRequested = isset($request->search['date_requested']) && !empty(trim($request->search['date_requested'])) ? $request->search['date_requested'] : false;
        $status = isset($request->search['status']) && !empty(trim($request->search['status'])) ? $request->search['status'] : false;
        $formType = isset($request->search['form_type']) && !empty(trim($request->search['form_type'])) ? $request->search['form_type'] : false;

        $searching = $keyword || $dateRequested || $status || $formType;

        if($keyword){
            $gatePassRequests = $gatePassRequests->where(function($query) use ($keyword) {
                $query->where('users.first_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('users.last_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('endorsers_list.first_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('endorsers_list.last_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('approvers_list.first_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('approvers_list.last_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('requests.guest', 'LIKE', '%' . $keyword . '%');
            });
        }

        if($dateRequested){
            $gatePassRequests = $gatePassRequests->whereRaw('CAST(requests.created_at as DATE) = ?', [$dateRequested]);
        }

        if($status){
            $gatePassRequests = $gatePassRequests->where('requests.status', $status);
        }

        $table['data'] = $gatePassRequests->offset(($request->size * $request->currentPage) - $request->size)
                                        ->limit($request->size)
                                        ->get();

        $table['data']->map(function($req){
            $isVehicle = $req->vehicle instanceof VehicleGatePassRequest;
            $isItem = $req->items->count();
            $req->form_type = $isVehicle && $isItem ? 'Vehicle & Item' : ($isVehicle ? 'Vehicle' : ($isItem ? 'Item' : 'Itinerary'));

            return $req;
        });

        if($formType){
            $table['data'] = $table['data']->filter(function($req) use ($formType){
                return $req->form_type == $formType;
            });
        }
        $table['total'] = $searching? 
                                $table['data']->count() 
                                : GatePassRequest::where('user_id', $id)->count();
        $table['data'] = array_merge($table['data']->toArray());
        return $table;
    }

    public function validateVehicleUsage($vehicle){
        $vehicleInUse = GatePassRequest::select('requests.id')->whereIn('requests.status', array('pending', 'endorsement', 'approval'))->join('vehicle_requests', 'requests.id', '=', 'vehicle_requests.request_id')->join('vehicles', 'vehicles.id', '=', 'vehicle_requests.vehicle_id')->where('vehicles.id', $vehicle['id'])->count();

        return $vehicleInUse ? response()->json(['in_use' => true]) : response()->json(['in_use' => false]);
    }
}