<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\GatePass\Auth\JWTAuthenticator;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $jwtAuth;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->jwtAuth = new JWTAuthenticator();
        // $this->middleware('guest', ['except' => 'logout']);
    }

    public function login(Request $request){
        return $this->jwtAuth
                    ->apiLogin($request->only('username', 'password'));
    }

    public function getUser(){
        return $this->jwtAuth
                    ->getUser();
    }

    public function changePassword(Request $request){
        return $this->jwtAuth
                    ->changePassword($request);
    }
}
