<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\Vehicle;
use App\GatePassRequest;
use App\VehicleGatePassRequest;
use App\ItemGatePassRequest;
use Carbon\Carbon;


class RequestMonitoringController extends Controller
{      
    
    public function getrequests(Request $request){
        // dd($request->all()); 
	    $table = array();
        $gatePassRequests = array();        
        $monitoring_status = array();
        $items = array();
        $prev_monitoring_status = "";

	    
        $date_from = $request->filter['date_from_final'] ?  $request->filter['date_from_final'] : false;
        $date_to = $request->filter['date_to_final'] ?  $request->filter['date_to_final'] : false;
        $form_type = $request->filter['formTypes'] ? $request->filter['formTypes'] : false;
        $status = $request->filter['status'] ? $request->filter['status'] : "";
        $keyword = $request->filter['keyword'] ?  $request->filter['keyword'] : false;
        // dd($date_to);
        $requests = GatePassRequest::selectRaw('requests.id, 
                                                DATE_FORMAT(requests.departured_at, "%c/%e/%Y") as departured_date,
                                                DATE_FORMAT(requests.returned_at, "%c/%e/%Y") as returned_date,     
                                                TIME(requests.departured_at) as departured_time, 
                                                TIME(requests.returned_at) as returned_time,
                                                requests.endorsed_by,
                                                requests.approved_by,
                                                requests.status,
                                                requests.monitoring_status,
                                                users.first_name as first_name,
                                                users.last_name as last_name,
        						  				CONCAT(users.first_name, " ", users.last_name) as user_name,
                                                vehicles.name as vehicle_name,
                                                vehicles.plate_number as plate_number')  
        			->join('users', 'users.id', '=', 'requests.user_id')
                    ->leftJoin('users as endorsers_list', 'requests.endorsed_by', '=', 'endorsers_list.id')
                    ->leftJoin('users as approvers_list', 'requests.approved_by', '=', 'approvers_list.id')
                    ->leftJoin('vehicle_requests', 'requests.id', '=' , 'vehicle_requests.request_id')
                    ->leftJoin('vehicles', 'vehicles.id', '=', 'vehicle_requests.vehicle_id')                  
                    ->where([ ['requests.status', '=', 'approved'], ['requests.monitoring_status', '!=', 'completed'] , ]);


         if($keyword){
            $requests = $requests->where(function($query) use ($keyword) {
                $query->where('users.first_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('users.last_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('endorsers_list.first_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('endorsers_list.last_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('approvers_list.first_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('approvers_list.last_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('vehicles.name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('vehicles.plate_number', 'LIKE', '%' . $keyword . '%');
            });
        }

        if($date_from){           
            $requests->where(function($query) use ($date_from){
                    $query->whereRaw('CAST(requests.departured_at as DATE) >= ?', [$date_from])
                          ->orWhereRaw('CAST(requests.returned_at as DATE) >= ?', [$date_from]);                        
                });
        }

        if($date_to){           
            $requests->where(function($query) use ($date_to){
                    $query->whereRaw('CAST(requests.departured_at as DATE) <= ?', [$date_to])
                          ->orWhereRaw('CAST(requests.returned_at as DATE) <= ?', [$date_to]);                        
                });
        }

        if($status){
            $requests->where('requests.monitoring_status','=', $status);
        }
       

        $table['data'] = $requests->offset(($request->size * $request->currentPage) - $request->size)
								   ->limit($request->size)                                   
								   ->get();

        $table['data']->map(function($req){
            $isVehicle = $req->vehicle instanceof VehicleGatePassRequest;
            $isItem = $req->items->count();
            $req->form_type = $isVehicle && $isItem ? 'Vehicle & Item' : ($isVehicle ? 'Vehicle' : ($isItem ? 'Item' : 'Itinerary'));
            return $req;
        });

        if($form_type){
            $table['data'] = $table['data']->filter(function($req) use ($form_type){
                return $req->form_type == $form_type;
            });
        }

        // $total = $keyword ? $requests->count() : GatePassRequest::count(array('id'));         
        
        foreach ($table['data'] as $gatepass_request) {
            $allItems = '';

            $prev_monitoring_status = $gatepass_request->monitoring_status;
            $vehicle_request = $gatepass_request->vehicle ? \App\Vehicle::find($gatepass_request->vehicle->vehicle_id) : null;
            $vehicle_details = '';
            $item_request = $gatepass_request->items;
            $vehicle_details = ''; 

            if(isset($search) && !empty($search)){
                dd("item");
                $item_request = \App\ItemGatePassRequest::where(function($query) use ($search){
                    $query->where('item_requests.name', 'LIKE', '%' . $search . '%')
                          ->orWhere('item_requests.quantity', 'LIKE', '%' . $search . '%');
                });
            }

            if($vehicle_request instanceof \App\Vehicle && $item_request->count()){
                    foreach($item_request as $item){
                        $items[] = $item->name . ' - ' . $item->quantity;
                    }
                    $vehicle_details = $vehicle_request->name." ( ".$vehicle_request->plate_number." )";
                    $gatePassRequests[] = array(     //Vehicle & Item   
                                        "id" =>  $gatepass_request->id,                      
                                        "form_type" => "Vehicle & Item",
                                        "user_name" => $gatepass_request->user_name,
                                        "vehicle_name" => $vehicle_request ? $vehicle_request->name : '',
                                        "plate_number" => $vehicle_request ? $vehicle_request->plate_number : '',
                                        "vehicle_details" => $vehicle_request ? $vehicle_details : '',
                                       "endorsed_by" =>$gatepass_request->endorsed_by ? $this->getName($gatepass_request->endorsed_by) : "",
                                        "approved_by" => $gatepass_request->approved_by ? $this->getName($gatepass_request->approved_by) : "" ,
                                        "item_qty" => implode(", ", $items),
                                        "departured_date" => $gatepass_request->departured_date,
                                        "departured_time" => $gatepass_request->departured_time,
                                        "returned_date" =>  $gatepass_request->returned_date,
                                        "returned_time" =>  $gatepass_request->returned_time,
                                        "status" => $gatepass_request->monitoring_status
                                    );
                    unset($items);
                    $items = array();
                    continue;
            }
            if($vehicle_request instanceof \App\Vehicle){
                    $gatePassRequests[] = array(     //Vehicle  
                                        "id" =>  $gatepass_request->id,                            
                                        "form_type" => "Vehicle",
                                        "user_name" => $gatepass_request->user_name,
                                        "vehicle_name" => $vehicle_request ? $vehicle_request->name : '',
                                        "plate_number" => $vehicle_request ? $vehicle_request->plate_number : '',
                                        "vehicle_details" => $vehicle_request ? $vehicle_details : '',
                                       "endorsed_by" =>$gatepass_request->endorsed_by ? $this->getName($gatepass_request->endorsed_by) : "",
                                        "approved_by" => $gatepass_request->approved_by ? $this->getName($gatepass_request->approved_by) : "" ,
                                        "item_qty" => "N/A",
                                        "departured_date" => $gatepass_request->departured_date,
                                        "departured_time" => $gatepass_request->departured_time,
                                        "returned_date" =>  $gatepass_request->returned_date,
                                        "returned_time" =>  $gatepass_request->returned_time,
                                        "status" => $gatepass_request->monitoring_status
                                    );
                    continue;
            }
            if($item_request->count()){
                    foreach($item_request as $item){
                        $items[] = $item->name . ' - ' . $item->quantity;
                    }
                    $gatePassRequests[] = array(     // Item   
                                        "id" =>  $gatepass_request->id,                          
                                        "form_type" => "Item",
                                        "user_name" => $gatepass_request->user_name,
                                        "vehicle_name" => "N/A",
                                        "plate_number" => "N/A",
                                        "vehicle_details" => "N/A",
                                       "endorsed_by" =>$gatepass_request->endorsed_by ? $this->getName($gatepass_request->endorsed_by) : "",
                                        "approved_by" => $gatepass_request->approved_by ? $this->getName($gatepass_request->approved_by) : "" ,
                                        "item_qty" => implode(", ", $items),
                                        "departured_date" => $gatepass_request->departured_date,
                                        "departured_time" => $gatepass_request->departured_time,
                                        "returned_date" =>  $gatepass_request->returned_date,
                                        "returned_time" =>  $gatepass_request->returned_time,
                                        "status" => $gatepass_request->monitoring_status
                                    );
                    unset($items);
                    $items = array();
                    continue;
            }

            $gatePassRequests[] = array(     //Itinerary 
                                "id" =>  $gatepass_request->id, 
                                "form_type" => "Itinerary",
                                "user_name" => $gatepass_request->user_name,
                                "vehicle_name" => "N/A",
                                "plate_number" => "N/A",
                                "vehicle_details" => "N/A",
                                "endorsed_by" =>$gatepass_request->endorsed_by ? $this->getName($gatepass_request->endorsed_by) : "",
                                "approved_by" => $gatepass_request->approved_by ? $this->getName($gatepass_request->approved_by) : "" ,
                                "item_qty" => "N/A",
                                "departured_date" => $gatepass_request->departured_date,
                                "departured_time" => $gatepass_request->departured_time,
                                "returned_date" =>  $gatepass_request->returned_date,
                                "returned_time" =>  $gatepass_request->returned_time,
                                "status" => $gatepass_request->monitoring_status
                            );

        } 

        $gatePassRequests = collect($gatePassRequests);
        $gatePassRequests = $gatePassRequests->unique('id');

        $total =  ( $keyword || $date_from || $date_to || $form_type || $status ) ? $gatePassRequests->count() : GatePassRequest::where([ ['requests.status', '=', 'approved'], ['requests.monitoring_status', '!=', 'completed'] , ])->count(array('id'));  
             
        return response()->json(array("total"=>$total,"requests" => $gatePassRequests->toArray()));

    }

     public function getActualRecords(Request $request){
        // dd($request->all());
        $table = array();
        $gatePassRequests = array();        
        $monitoring_status = array();
        $items = array();
        $prev_monitoring_status = "";

        
        $date_from = $request->filter['date_from_final'] ?  $request->filter['date_from_final'] : false;
        $date_to = $request->filter['date_to_final'] ?  $request->filter['date_to_final'] : false;
        $form_type = $request->filter['formTypes'] ? $request->filter['formTypes'] : false;
        $status = $request->filter['status'] ? $request->filter['status'] : "";
        $keyword = $request->filter['keyword'] ?  $request->filter['keyword'] : false;
        // dd($date_to);
        $requests = GatePassRequest::selectRaw('requests.id, 
                                                DATE_FORMAT(requests.actual_departured_at, "%c/%e/%Y") as departured_date,
                                                DATE_FORMAT(requests.actual_returned_at, "%c/%e/%Y") as returned_date,     
                                                TIME(requests.actual_departured_at) as departured_time, 
                                                TIME(requests.actual_returned_at) as returned_time,
                                                requests.endorsed_by,
                                                requests.approved_by,
                                                requests.status,
                                                requests.monitoring_status,
                                                users.first_name as first_name,
                                                users.last_name as last_name,
                                                CONCAT(users.first_name, " ", users.last_name) as user_name,
                                                vehicles.name as vehicle_name,
                                                vehicles.plate_number as plate_number')  
                    ->join('users', 'users.id', '=', 'requests.user_id')
                    ->leftJoin('users as endorsers_list', 'requests.endorsed_by', '=', 'endorsers_list.id')
                    ->leftJoin('users as approvers_list', 'requests.approved_by', '=', 'approvers_list.id')
                    ->leftJoin('vehicle_requests', 'requests.id', '=' , 'vehicle_requests.request_id')
                    ->leftJoin('vehicles', 'vehicles.id', '=', 'vehicle_requests.vehicle_id') 
                    ->where([ ['requests.status', '=', 'approved'], ['requests.monitoring_status', '=', 'completed'] , ]);


         if($keyword){
            $requests = $requests->where(function($query) use ($keyword) {
                $query->where('users.first_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('users.last_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('endorsers_list.first_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('endorsers_list.last_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('approvers_list.first_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('approvers_list.last_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('vehicles.name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('vehicles.plate_number', 'LIKE', '%' . $keyword . '%');
            });
        }

       if($date_from){           
            $requests->where(function($query) use ($date_from){
                    $query->whereRaw('CAST(requests.actual_departured_at as DATE) >= ?', [$date_from])
                          ->orWhereRaw('CAST(requests.actual_returned_at as DATE) >= ?', [$date_from]);                        
                });
        }

        if($date_to){           
            $requests->where(function($query) use ($date_to){
                    $query->whereRaw('CAST(requests.actual_departured_at as DATE) <= ?', [$date_to])
                          ->orWhereRaw('CAST(requests.actual_returned_at as DATE) <= ?', [$date_to]);                        
                });
        }

        if($status){
            $requests->where('requests.monitoring_status','=', $status);
        }
       

        $table['data'] = $requests->offset(($request->size * $request->currentPage) - $request->size)
                                   ->limit($request->size)                                   
                                   ->get();

        $table['data']->map(function($req){
            $isVehicle = $req->vehicle instanceof VehicleGatePassRequest;
            $isItem = $req->items->count();
            $req->form_type = $isVehicle && $isItem ? 'Vehicle & Item' : ($isVehicle ? 'Vehicle' : ($isItem ? 'Item' : 'Itinerary'));
            return $req;
        });

        if($form_type){
            $table['data'] = $table['data']->filter(function($req) use ($form_type){
                return $req->form_type == $form_type;
            });
        }

        // $total = $keyword ? $requests->count() : GatePassRequest::count(array('id'));         
        
        foreach ($table['data'] as $gatepass_request) {
            $allItems = '';

            $prev_monitoring_status = $gatepass_request->monitoring_status;
            $vehicle_request = $gatepass_request->vehicle ? \App\Vehicle::find($gatepass_request->vehicle->vehicle_id) : null;
            $item_request = $gatepass_request->items;
            $vehicle_details = '';
            if(isset($search) && !empty($search)){
                dd("item");
                $item_request = \App\ItemGatePassRequest::where(function($query) use ($search){
                    $query->where('item_requests.name', 'LIKE', '%' . $search . '%')
                          ->orWhere('item_requests.quantity', 'LIKE', '%' . $search . '%');
                });
            }

            if($vehicle_request instanceof \App\Vehicle && $item_request->count()){
                    foreach($item_request as $item){
                        $items[] = $item->name . ' - ' . $item->quantity;
                    }
                    $vehicle_details = $vehicle_request->name." ( ".$vehicle_request->plate_number." )";
                    $gatePassRequests[] = array(     //Vehicle & Item   
                                        "id" =>  $gatepass_request->id,                      
                                        "form_type" => "Vehicle & Item",
                                        "user_name" => $gatepass_request->user_name,
                                        "vehicle_name" => $vehicle_request ? $vehicle_request->name : '',
                                        "plate_number" => $vehicle_request ? $vehicle_request->plate_number : '',
                                        "vehicle_details" => $vehicle_request ? $vehicle_details : '',
                                       "endorsed_by" =>$gatepass_request->endorsed_by ? $this->getName($gatepass_request->endorsed_by) : "",
                                        "approved_by" => $gatepass_request->approved_by ? $this->getName($gatepass_request->approved_by) : "" ,
                                        "item_qty" => implode(", ", $items),
                                        "departured_date" => $gatepass_request->departured_date,
                                        "departured_time" => $gatepass_request->departured_time,
                                        "returned_date" =>  $gatepass_request->returned_date,
                                        "returned_time" =>  $gatepass_request->returned_time,
                                        "status" => $gatepass_request->monitoring_status
                                    );
                    unset($items);
                    $items = array();
                    continue;
            }
            if($vehicle_request instanceof \App\Vehicle){
                    $gatePassRequests[] = array(     //Vehicle  
                                        "id" =>  $gatepass_request->id,                            
                                        "form_type" => "Vehicle",
                                        "user_name" => $gatepass_request->user_name,
                                        "vehicle_name" => $vehicle_request ? $vehicle_request->name : '',
                                        "plate_number" => $vehicle_request ? $vehicle_request->plate_number : '',
                                        "vehicle_details" => $vehicle_request ? $vehicle_details : '',
                                       "endorsed_by" =>$gatepass_request->endorsed_by ? $this->getName($gatepass_request->endorsed_by) : "",
                                        "approved_by" => $gatepass_request->approved_by ? $this->getName($gatepass_request->approved_by) : "" ,
                                        "item_qty" => "N/A",
                                        "departured_date" => $gatepass_request->departured_date,
                                        "departured_time" => $gatepass_request->departured_time,
                                        "returned_date" =>  $gatepass_request->returned_date,
                                        "returned_time" =>  $gatepass_request->returned_time,
                                        "status" => $gatepass_request->monitoring_status
                                    );
                    continue;
            }
            if($item_request->count()){
                    foreach($item_request as $item){
                        $items[] = $item->name . ' - ' . $item->quantity;
                    }
                    $gatePassRequests[] = array(     // Item   
                                        "id" =>  $gatepass_request->id,                          
                                        "form_type" => "Item",
                                        "user_name" => $gatepass_request->user_name,
                                        "vehicle_name" => "N/A",
                                        "plate_number" => "N/A",
                                        "vehicle_details" => "N/A",
                                        "endorsed_by" =>$gatepass_request->endorsed_by ? $this->getName($gatepass_request->endorsed_by) : "",
                                        "approved_by" => $gatepass_request->approved_by ? $this->getName($gatepass_request->approved_by) : "" ,
                                        "item_qty" => "N/A",
                                        "departured_date" => $gatepass_request->departured_date,
                                        "departured_time" => $gatepass_request->departured_time,
                                        "returned_date" =>  $gatepass_request->returned_date,
                                        "returned_time" =>  $gatepass_request->returned_time,
                                        "status" => $gatepass_request->monitoring_status
                                    );
                    unset($items);
                    $items = array();
                    continue;
            }

            $gatePassRequests[] = array(     //Itinerary 
                                "id" =>  $gatepass_request->id, 
                                "form_type" => "Itinerary",
                                "user_name" => $gatepass_request->user_name,
                                "vehicle_name" => "N/A",
                                "plate_number" => "N/A",
                                "vehicle_details" => "N/A",
                                "endorsed_by" =>$gatepass_request->endorsed_by ? $this->getName($gatepass_request->endorsed_by) : "",
                                "approved_by" => $gatepass_request->approved_by ? $this->getName($gatepass_request->approved_by) : "" ,
                                "item_qty" => "N/A",
                                "departured_date" => $gatepass_request->departured_date,
                                "departured_time" => $gatepass_request->departured_time,
                                "returned_date" =>  $gatepass_request->returned_date,
                                "returned_time" =>  $gatepass_request->returned_time,
                                "status" => $gatepass_request->monitoring_status
                            );

        } 

        $gatePassRequests = collect($gatePassRequests);
        $gatePassRequests = $gatePassRequests->unique('id');

        $total =  ( $keyword || $date_from || $date_to || $form_type || $status ) ? $gatePassRequests->count() : GatePassRequest::where([ ['requests.status', '=', 'approved'], ['requests.monitoring_status', '=', 'completed'] , ])->count(array('id'));  
             
        return response()->json(array("total"=>$total,"requests" => $gatePassRequests->toArray()));

    }

    public function activateDepartured($request_id){
        date_default_timezone_set("Asia/Manila");
        $request = \App\GatePassRequest::find($request_id);
        $time = time();
        $departed = date("Y-m-d H:i:s", $time);
        if($request->monitoring_status === 'pending' && $request->monitoring_status != 'completed'){
            $request->monitoring_status = 'ongoing';
            $request->actual_departured_at = $departed;
            $request->save();
            return response()->json(['message' => 'Successfully departured.'], 200);
        }else{
            return response()->json(['message' => 'Already departed.'], 200); 
        }
    }

    public function activateReturned($request_id){
       date_default_timezone_set("Asia/Manila");
        $request = \App\GatePassRequest::find($request_id);
        $time = time();
        $returned = date("Y-m-d H:i:s", $time);
        // echo $returned;
        if($request->monitoring_status === 'ongoing' && $request->monitoring_status != 'pending'){
            $request->monitoring_status = 'completed';
            $request->actual_returned_at = $returned;
            $request->save();  
            return response()->json(['message' => 'Successfully returned.'], 200);
        }else{
            return response()->json(['message' => 'Already returned'], 200); 
        }
        
    }

    public function getName($id){
        $name = \App\User::find($id);
        $fullname = $name->first_name." ".$name->last_name;
        return $fullname;
    }

}