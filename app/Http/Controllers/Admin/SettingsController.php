<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Storage;
use Config;
use Carbon\Carbon;
use Validator;

class SettingsController extends Controller
{
    public function store(Request $request){
        
        $url = null;

        $validation = Validator::make($request->all(), [
            'image' => 'max:2048',
            'default_duration' => 'required|numeric|min:0',
            'admin_email' => 'required',
        ]);

        if($validation->fails()){
            return response()->json(['error' => $validation->errors()->all()], 500);
        }

        if($request->file('image')){
            $extension = $request->file('image')->getClientOriginalExtension();

            $name = $request->file('image')->getClientOriginalName() . Carbon::now()->tz('Asia/Manila')->format("Y-m-d h:i:s a");
            
            if($extension == 'svg'){
                $file_name = md5($name) . "." . $extension;
                $image = $request->file('image')->storeAs('settings', $file_name, 'public');
            }else {
                $image = $request->file('image')->store('settings', 'public');
            }

            
            $url = Storage::url($image);

        }
        if($url) {
            Config::write('cone_settings', ['site_logo' => $url, 'default_duration' => $request->default_duration, 'admin_email' => $request->admin_email]);
        } else {
            Config::write('cone_settings', ['default_duration' => $request->default_duration, 'admin_email' => $request->admin_email]);
        }


        $settings = array(
            'site_logo' => $url ? $url : config('cone_settings.site_logo'),
            'default_duration' => $request->default_duration,
            'admin_email' => $request->admin_email
        );

    	return $settings;
       
    }

    public function getSettings(Request $request){
    	$settings = array(
    	    'site_logo' => config('cone_settings.site_logo'),
            'default_duration' => (int)(config('cone_settings.default_duration')),
            'admin_email' => config('cone_settings.admin_email'),
    	);
    	return response()->json($settings);
    }
}