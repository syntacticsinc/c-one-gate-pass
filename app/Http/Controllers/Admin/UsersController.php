<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GatePass\Repositories\UserRepository;
use App\Role;

class UsersController extends Controller
{

    protected $userRepository;

    public function __construct(UserRepository $userRepository){
        $this->userRepository = $userRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->userRepository
                    ->create($request->all());
    }

    public function update($username, Request $request){
        return $this->userRepository
                    ->update($username, $request->all());
    }

    public function usersForTable(Request $request){
        return $this->userRepository
                    ->getUsersForTable($request);
    }

    public function getFieldOptions(){
        $roles = Role::all(['id', 'name', 'display_name'])->toArray();
        return response()->json([
                'roles' => $roles,
            ]);
    }

    public function search($keyword, Request $request){
        if((bool)$request->assoc){
            return $this->userRepository
                        ->getFollowersOf($request->username, $keyword);    
        }
        if((bool)$request->admin){
            return $this->userRepository
                        ->getAllUsers($keyword);
        }
        return $this->userRepository
                    ->searchNotIn($keyword, $request->selected_users, $request->username ? $request->username : '');
    }

    public function assign(Request $request){
        return $this->userRepository
                    ->assign($request->all());
    }

    public function getAssignedUsers($username){
        return $this->userRepository
                    ->getAssignedUsersFor($username);
    }

    public function getUserByUsername($username){
        return $this->userRepository
                    ->getUserByUsername($username);
    }
    public function getUserDataByUsername($username){
        return $this->userRepository
                    ->getUserDataByUsername($username);
    }

    public function deleteUsers(Request $request){
        return $this->userRepository
                    ->deleteUsers($request->all());
    }

    public function resetUsers(Request $request){
        return $this->userRepository
                    ->resetUsers($request->all());
    }
}