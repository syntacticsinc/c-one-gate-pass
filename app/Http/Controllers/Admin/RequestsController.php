<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GatePassRequest;
use App\VehicleGatePassRequest;
use App\ItemGatePassRequest;
use Illuminate\Support\Facades\DB;

class RequestsController extends Controller
{
     public function get(Request $request){

     	$search = $request->search ?  $request->search : false;
        $search['date_requested'] = isset($search['date_requested']) ? $search['date_requested'] : false;
     	$searching = $search['keyword'] || $search['date_requested'] || $search['status'] || $search['form_type'];


		$gatePassRequests = GatePassRequest::selectRaw('requests.id, 
		        DATE_FORMAT(requests.created_at, "%c/%e/%Y") as date_requested,
		        DATE_FORMAT(requests.departured_at, "%c/%e/%Y") as departure_date,     
		        TIME(requests.departured_at) as departure_time,  
		        DATE(requests.returned_at) as return_date,
		        TIME(requests.returned_at) as return_time,
                requests.guest,
		        IFNULL(CONCAT(endorsers_list.first_name, " ", endorsers_list.last_name), "N/A") as endorser,
		        IFNULL(CONCAT(approvers_list.first_name, " ", approvers_list.last_name), "N/A") as approver,
		        CONCAT(UCASE(SUBSTRING(requests.status, 1, 1)),SUBSTRING(requests.status, 2)) as status,
		        users.first_name as first_name,
		        users.last_name as last_name,
		        CONCAT(users.first_name, " ", users.last_name) as employee_name')           
		->join('users', 'users.id', '=', 'requests.user_id')
		->leftJoin('users as endorsers_list', 'requests.endorsed_by', '=', 'endorsers_list.id')
		->leftJoin('users as approvers_list', 'requests.approved_by', '=', 'approvers_list.id');
        // ->whereIn('status', ['approval', 'approved', 'declined', 'expired']);

        if($request->monitoringStatus){
            $gatePassRequests->where('requests.monitoring_status', $request->monitoringStatus);
        }

         if($search['keyword']){

             $gatePassRequests->where('users.first_name', 'LIKE', '%' . $search['keyword'] . '%')
                        ->orWhere('users.last_name', 'LIKE', '%' . $search['keyword'] . '%')
                        ->orWhere('endorsers_list.first_name', 'LIKE', '%' . $search['keyword'] . '%')
                        ->orWhere('endorsers_list.last_name', 'LIKE', '%' . $search['keyword'] . '%')
                        ->orWhere('approvers_list.first_name', 'LIKE', '%' . $search['keyword'] . '%')
                        ->orWhere('approvers_list.last_name', 'LIKE', '%' . $search['keyword'] . '%')
                        ->orWhere('requests.guest', 'LIKE', '%' . $search['keyword'] . '%');
        }

        if($search['date_requested']){
            $gatePassRequests = $gatePassRequests->whereRaw('CAST(requests.created_at as DATE) = ?', [$search['date_requested']]);
        }

        if($search['status']){
            $gatePassRequests = $gatePassRequests->where('requests.status', $search['status']);
        }


        $gatePassRequests =  $gatePassRequests->offset(($request->size * $request->currentPage) - $request->size)
                                        ->limit($request->size)
                                        ->get();
       

		$gatePassRequests->map(function($req){
		    $isVehicle = $req->vehicle instanceof VehicleGatePassRequest;
		    $isItem = $req->items->count();
		    $req->form_type = $isVehicle && $isItem ? 'Vehicle & Item' : ($isVehicle ? 'Vehicle' : ($isItem ? 'Item' : 'Itinerary'));

		    return $req;
		});


        if($search['form_type']){
        	$form_type = $search['form_type'];
            $gatePassRequests = $gatePassRequests->filter(function($req) use ($form_type){
                 return $req->form_type == $form_type;              
            });
        }

        $table = array(
            'data' => $gatePassRequests,
            'total' => $searching ?  $gatePassRequests->count() : GatePassRequest::count()
        );

        $table['data'] = array_merge($table['data']->toArray());
     	return $table;


     }
}