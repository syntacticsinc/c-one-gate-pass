<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GatePassRequest;
use App\VehicleGatePassRequest;
use App\ItemGatePassRequest;

class DashboardController extends Controller
{
    public function getTotalRequests(){
    	return GatePassRequest::count();
    }

    public function getTotalItemRequests(){
    	
    	return ItemGatePassRequest::distinct('request_id')->count('request_id');
    }

    public function getTotalVehicleRequests(){
    	return VehicleGatePassRequest::count();
    }

    public function getAllTotalRequests(){
    	$total = array(
    		'requests' => $this->getTotalRequests(),
    		'itemRequests' => $this->getTotalItemRequests(),
    		'vehicleRequest' => $this->getTotalVehicleRequests(),
    	);

    	return response()->json($total);
    }

}