<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GatePass\Repositories\VehicleRepository;
use App\Vehicle;

class VehiclesController extends Controller
{
    protected $vehicleRepository;

    public function __construct(VehicleRepository $vehicleRepository){
        $this->vehicleRepository = $vehicleRepository;
    }

    public function store(Request $request){
    	return $this->vehicleRepository
                    ->create($request->all());
    }

    public function getVehicles(Request $request){
    	return $this->vehicleRepository
    	            ->getVehicles($request);
    }

    public function getVehicle($id){
        return $this->vehicleRepository
                    ->getVehicle($id);
    }

    public function delete($id){
        return $this->vehicleRepository
                    ->delete($id);
    }

    public function search(Request $request){
        $keyword = $request->keyword ? $request->keyword : '';

        return $this->vehicleRepository
                    ->search($keyword);
    }
}