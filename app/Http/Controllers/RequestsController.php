<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\GatePass\Repositories\RequestsRepository;
use App\GatePass\GatePass;

class RequestsController extends Controller
{

	protected $requestsRepo;
    protected $gatepass;

	public function __construct(RequestsRepository $requestsRepo, GatePass $gatepass){
		$this->requestsRepo = $requestsRepo;
        $this->gatepass = $gatepass;
	}

    public function getDefaultTime(){
        return array(
            'departure_date' => \Carbon\Carbon::now()->tz('Asia/Manila')->format('Y-m-d'),
            'departure_time' => \Carbon\Carbon::now()->tz('Asia/Manila')->format('H:i'),
            'return_date' => \Carbon\Carbon::now()->tz('Asia/Manila')->addHours(\Config::get('cone_settings.default_duration'))->format('Y-m-d'),
            'return_time' => \Carbon\Carbon::now()->tz('Asia/Manila')->addHours(\Config::get('cone_settings.default_duration'))->format('H:i'),
            'year' => \Carbon\Carbon::now()->tz('Asia/Manila')->format('Y'),
            'month' => \Carbon\Carbon::now()->tz('Asia/Manila')->format('m'),
            'day' => \Carbon\Carbon::now()->tz('Asia/Manila')->format('d')
            );
    }

    public function store(Request $request){
    	return $this->gatepass
                    ->makeRequest($request->all());
    }

    public function getNotificationsFor($username, Request $request){
        return $this->gatepass
                    ->getNotificationsFor($username, $request);
    }

    public function clearNotifications($username, Request $request){
        return $this->gatepass
                    ->clearNotificationsOf($request->notifications, $username);
    }

    public function endorse($id, Request $request){
        return $this->gatepass
                    ->endorse($id, $request);
    }

    public function approve($id, Request $request){
        return $this->gatepass
                    ->approve($id, $request);
    }

    public function decline($id, Request $request){
        return $this->gatepass
                    ->decline($id, $request);
    }

    public function getRequestInfo($id, Request $request){
        return $this->gatepass
                    ->getRequestInfo($id);
    }

    public function removeNotificationOf($username, $notificationId, Request $request){
        return $this->gatepass
                    ->clearNotificationsOf([$notificationId], $username);
    }

    public function getAllRequests($id, Request $request){
        return $this->gatepass
                    ->getAllRequestsOf($id, $request);
    }

    public function getTypedRequests($id, $type, Request $request){
        return $this->gatepass
                    ->getTypedRequests($id, $type, $request);
    }

    public function bulkEndorse($endorser, Request $request){
        return $this->gatepass
                    ->bulkEndorse($endorser, $request);
    }

    public function bulkApprove($approver, Request $request){
        return $this->gatepass
                    ->bulkApprove($approver, $request);
    }

    public function bulkDecline($decliner, Request $request){
        return $this->gatepass
                    ->bulkDecline($decliner, $request);
    }

    public function validateVehicleUsage(Request $request){
        return $this->gatepass->validateVehicleUsage($request->vehicle);                                        
    }

}
