<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $gatePass = $this->app->make('App\GatePass\GatePass');
        $schedule->call(function () {
                    // DB::table('recent_users')->delete();
                    $gatePassRequests = \App\GatePassRequest::where('expiration', '<', \Carbon\Carbon::now())
                                                        ->orWhere('departured_at', '<', \Carbon\Carbon::now())
                                                        ->where('monitoring_status', 'pending');
                    if($gatePassRequests->count()){
                        \DB::update('DELETE FROM `notification_user` WHERE `notification_user`.`request_id` IN (' . implode(', ', array_column($gatePassRequests->get(['id'])->toArray(), 'id')) . ')');
                        $gatePassRequests->update(['status' => 'expired']);
                        $gatePassRequests = $gatePassRequests->get();
                        $gatePassRequests->map(function($request){
                            $request->user->notifications()
                                            ->save($request, ['read_status' => false]);
                        });
                    }

                })->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
