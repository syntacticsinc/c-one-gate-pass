<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Approver extends Model
{
    protected $table = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function employees(){
    	return $this->belongsToMany('App\User', 'user_approvers', 'approver_id', 'user_id');
    }

    public function approvedRequests(){
        return $this->hasMany('App\GatePassRequest', 'approved_by', 'id');
    }

    public function notifications(){
        return $this->belongsToMany('App\GatePassRequest', 'notification_user', 'user_id', 'request_id')
                    ->withPivot(['read_status']);
    }

    public function getFullNameAttribute(){
        return $this->first_name . ' ' . $this->last_name;
    }
}
