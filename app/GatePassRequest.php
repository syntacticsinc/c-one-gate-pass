<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GatePassRequest extends Model
{
    protected $table = 'requests';

    protected $fillable = [
    	'user_id',
    	'endorsed_by',
    	'approved_by',
    	'departured_at',
    	'returned_at',
        'destination',
    	'purpose',
    	'status',
    	'reason',
        'endorsement_expiration',
        'approval_expiration',
    ];

    protected $dates = [
        'departured_at',
        'returned_at',
        'created_at',
        'updated_at',
        'endorsement_expiration',
        'approval_expiration',
        'expiration',
        'actual_departured_at',
        'actual_returned_at',
    ];

    public function vehicle(){
        return $this->hasOne('App\VehicleGatePassRequest', 'request_id', 'id');
    }

    public function items(){
        return $this->hasMany('App\ItemGatePassRequest', 'request_id', 'id');
    }

    public function notifications(){
        return $this->belongsToMany('App\User', 'notification_user', 'request_id', 'user_id')
                    ->withPivot(['read_status', 'status']);
    }

    public function endorser(){
        return $this->belongsTo('App\Endorser', 'endorsed_by', 'id');
    }

    public function approver(){
        return $this->belongsTo('App\Approver', 'approved_by', 'id');
    }

    // public function sender(){
    //     return $this->belongsTo('App\User', 'sender_id', 'id');
    // }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
