<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemGatePassRequest extends Model
{
    protected $table = 'item_requests';
    protected $fillable = [
    	'request_id',
    	'name',
    	'quantity',
    ];
    public $timestamps = false;

    public function gatePassRequest(){
    	return $this->belongsTo('App\GatePassRequest');
    }
}
