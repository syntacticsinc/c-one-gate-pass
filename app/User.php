<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Laravel\Scout\Searchable;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'first_name',
        'last_name',
        'email', 
        'password',
        'department',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function endorsers(){
        return $this->belongsToMany('App\Endorser', 'user_endorsers', 'user_id', 'endorser_id');
    }

    public function approvers(){
        return $this->belongsToMany('App\Approver', 'user_approvers', 'user_id', 'approver_id');
    }

    public function searchableAs(){
        return 'users_index';
    }

    public function getFullNameAttribute(){
        return $this->first_name . ' ' . $this->last_name;
    }

    public function notifications(){
        return $this->belongsToMany('App\GatePassRequest', 'notification_user', 'user_id', 'request_id')
                    ->withPivot(['read_status', 'status']);
    }

    public function gatePassRequests(){
        return $this->hasMany('App\GatePassRequest', 'user_id', 'id');
    }

    public function endorsedRequest(){
        return $this->hasMany('App\GatePassRequest', 'endorsed_by', 'id');
    }

    public function approvedRequest(){
        return $this->hasMany('App\GatePassRequest', 'approved_by', 'id');
    }

    public function toSearchableArray(){
        return array_only($this->toArray(), [
                'id', 
                'username', 
                'first_name',
                'last_name',
                'email', 
                'password',
                'department',
            ]);
    }
}
