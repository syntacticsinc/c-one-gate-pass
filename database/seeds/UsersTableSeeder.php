<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        $users = array(
        		array(
        			'username' => 'admin',
        			'first_name' => 'Admin',
        			'last_name' => 'Admin',
        			'email' => 'admin@email.com',
        			'password' => \Hash::make('admin'),
                    'department' => 'Finance'
        			),
        		array(
        			'username' => 'approver',
        			'first_name' => 'Approver',
        			'last_name' => 'Approver',
        			'email' => 'approver@email.com',
        			'password' => \Hash::make('approver'),
                    'department' => 'Finance'
        			),
                array(
                    'username' => 'normal',
                    'first_name' => 'normal',
                    'last_name' => 'normal',
                    'email' => 'normal@email.com',
                    'password' => \Hash::make('normal'),
                    'department' => 'Finance'
                    ),
        	);

        \DB::table('users')->insert($users);

        $role = \App\Role::where('name', 'admin')->first();
        $user = \App\User::where('username', 'admin')->first();
        $user->attachRole($role);

        $role = \App\Role::where('name', 'normal')->first();
        $user = \App\User::where('username', 'approver')->first();
        $user->attachRole($role);

        $role = \App\Role::where('name', 'normal')->first();
        $user = \App\User::where('username', 'normal')->first();
        $user->attachRole($role);
    }
}
