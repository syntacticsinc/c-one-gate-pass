<?php

use Illuminate\Database\Seeder;

class VehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('vehicles')->delete();

        $vehicles = array(
        		array(
        				'name' => 'Vehicle 1',
        				'number' => '89271',
        				'plate_number' => 'KGB-123'
        			),
        		array(
        				'name' => 'Vehicle 2',
        				'number' => '54872',
        				'plate_number' => 'WRQ-547'
        			),
        		array(
        				'name' => 'Vehicle 3',
        				'number' => '81251',
        				'plate_number' => 'YEQ-748'
        			),
        	);

        \DB::table('vehicles')->insert($vehicles);
    }
}
