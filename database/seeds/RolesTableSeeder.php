<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->delete();

        $roles = array(
        		array(
        				'name' => 'admin',
        				'display_name' => 'Administrator',
        				'description' => 'App Administrator.',
        				'created_at' => \Carbon\Carbon::now(),
        				'updated_at' => \Carbon\Carbon::now(),
        			),
        		array(
        				'name' => 'monitoring',
        				'display_name' => 'Monitoring',
        				'description' => 'Record Monitoring.',
        				'created_at' => \Carbon\Carbon::now(),
        				'updated_at' => \Carbon\Carbon::now(),
        			),
        		array(
        				'name' => 'normal',
        				'display_name' => 'Normal User',
        				'description' => 'Normal User.',
        				'created_at' => \Carbon\Carbon::now(),
        				'updated_at' => \Carbon\Carbon::now(),
        			),
        	);

        \DB::table('roles')->insert($roles);
    }
}
