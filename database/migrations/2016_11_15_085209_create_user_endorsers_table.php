<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEndorsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_endorsers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')
                    ->unsigned();

            $table->integer('endorser_id')
                    ->unsigned();

            $table->foreign('endorser_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_endorsers');
    }
}
