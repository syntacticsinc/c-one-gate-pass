<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id')
                  ->unsigned();
            $table->integer('vehicle_id')
                  ->unsigned();
            $table->foreign('request_id')    
                    ->references('id')
                    ->on('requests')
                    ->onDelete('cascade');
            $table->foreign('vehicle_id')    
                    ->references('id')
                    ->on('vehicles')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_requests');
    }
}
