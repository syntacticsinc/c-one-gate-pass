<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')
                    ->nullable()
                   ->unsigned();
            $table->integer('endorsed_by')
                    ->nullable()
                    ->unsigned();

            $table->integer('approved_by')
                    ->nullable()
                    ->unsigned();

            $table->integer('declined_by')
                    ->nullable()
                    ->unsigned();

            $table->integer('sent_by')
                    ->nullable()
                    ->unsigned();

            $table->string('guest')
                    ->nullable();

            $table->timestamp('departured_at')
                    ->nullable();

            $table->timestamp('returned_at')
                    ->nullable();

            $table->text('purpose');

            $table->string('status');

            $table->string('monitoring_status');

            $table->timestamp('expiration')
                    ->nullable();

            $table->timestamp('actual_departured_at')
                    ->nullable();

            $table->timestamp('actual_returned_at')
                    ->nullable();

            $table->text('reason')
                    ->nullable();

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->foreign('endorsed_by')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->foreign('approved_by')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->foreign('declined_by')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->foreign('sent_by')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
