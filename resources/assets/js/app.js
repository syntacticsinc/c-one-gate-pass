
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */
import locale from 'element-ui/lib/locale/lang/en';
window.VueRouter = require('vue-router');
window.NProgress = require('nprogress');
window.filters = require('./filters/filters');
window.toastr = require('toastr');
window.ElementUI = require('element-ui');
// window.locale = require('element-ui/lib/locale/lang/en');
window.Notification = ElementUI.Notification;
window.MessageBox = ElementUI.MessageBox;
let auth = require('./services/Auth');
Vue.use(ElementUI, { locale });

function requireAuth(to, from, next) {
	if (!auth.loggedIn()) {
		next({
			path: '/login',
			query: { redirect: to.fullPath }
		});
	} else {
		next();
	}
}

function requireNormal(to, from, next) {
	if (!auth.loggedIn()) {
		next({
			path: '/login',
			query: { redirect: to.fullPath }
		});
	} else {
		if(localStorage.getItem('user.role') == 'admin') next({ path: '/admin/dashboard' });
		next();
	}
}

function requireAdmin(to, from, next) {
	if (!auth.loggedIn()) {
		next({
			path: '/login',
			query: { redirect: to.fullPath }
		});
	} else {
		if(localStorage.getItem('user.role') != 'admin') next({ path: '/404' });
		next();
	}
}

function requireMonitoring(to, from, next) {
	if (!auth.loggedIn()) {
		next({
			path: '/login',
			query: { redirect: to.fullPath }
		});
	} else {
		if(localStorage.getItem('user.role') != 'monitoring') next({ path: '/404' });
		next();
	}
}

window.displayErrors = function(errors){
	toastr.clear();
	let errorMessages = '';
	if(_.isArray(errors)){
		_.forEach(errors, (error, key) => {
			errorMessages += `${error}<br>`;
		})
		toastr.error(errorMessages, 'Whoops, looks like something went wrong.');
		return false;
	}
	toastr.error(errors, 'Ooops!');
}

window.requestFailedBox = function(doAction){
	MessageBox({
			title: 'Something went wrong!',
			message: 'An error occured while connecting to the server.',
			confirmButtonText: 'Try again',
			showCancelButton: true,
			cancelButtonText: 'Cancel',
		}).then((action) => {
			if(action == 'confirm'){
				doAction();
			}
		});
}

Vue.use(VueRouter);

let App = require('./views/App.vue');

Vue.component('request-form', require('./components/RequestForm.vue'));
Vue.component('request-notifications', require('./components/RequestNotifications.vue'));
Vue.component('request-notifications-dialog', require('./components/RequestNotificationsDialog.vue'));
Vue.component('request-records', require('./components/RequestRecords.vue'));
Vue.component('request-list', require('./components/RequestList.vue'));
Vue.component('front-end-menu', require('./components/FronEndMenu.vue'));
Vue.component('change-password-form', require('./components/ChangePasswordForm.vue'));
// Admin
Vue.component('create-user-form', require('./components/Admin/UserForm.vue'));
Vue.component('user-selection-control', require('./components/Admin/UserSelectionControl.vue'));

Vue.component('upload-site-logo', require('./components/Admin/UploadSiteLogo.vue'));
Vue.component('post-vehicle-form', require('./components/Admin/PostVehicleForm.vue'));
Vue.component('vehicles-list', require('./components/Admin/VehiclesList.vue'));
Vue.component('users-list', require('./components/Admin/UsersList.vue'));
Vue.component('admin-requests-list', require('./components/Admin/RequestsList.vue'));
Vue.component('admin-records-list', require('./components/Admin/RecordsList.vue'));

// Plugins
Vue.component(ElementUI.TimePicker.name, ElementUI.TimePicker);
Vue.component(ElementUI.Table.name, ElementUI.Table);
Vue.component(ElementUI.Pagination.name, ElementUI.Pagination);
Vue.component(ElementUI.DatePicker.name, ElementUI.DatePicker);
Vue.component(ElementUI.TimePicker.name, ElementUI.TimePicker);

Vue.filter('pendingNotificationText', filters.pendingNotificationText);
Vue.filter('approvedNotificationText', filters.approvedNotificationText);

let routes = [
	{ path: '/notifications', component: require('./views/RequestNotifications.vue'), alias: '/', beforeEnter: requireNormal },
	{ path: '/notifications/:id/endorse', component: require('./views/EndorseRequest.vue'), beforeEnter: requireAuth },
	{ path: '/notifications/:id/approve', component: require('./views/ApproveRequest.vue'), beforeEnter: requireAuth },
	{ path: '/request/:id/details', component: require('./views/RequestNotificationInfo.vue'), beforeEnter: requireAuth },
	{ path: '/make-request', component: require('./views/Request.vue'), beforeEnter: requireNormal },
	{ path: '/list', component: require('./views/RequestList.vue'), beforeEnter: requireNormal },
	{ path: '/endorsement-list', component: require('./views/EndorsementList.vue'), beforeEnter: requireNormal },
	{ path: '/approval-list', component: require('./views/ApprovalList.vue'), beforeEnter: requireNormal },
	{ path: '/records', component: require('./views/RequestRecords.vue'), beforeEnter: requireNormal },
	{ path: '/change-password', component: require('./views/ChangePassword.vue'), beforeEnter: requireAuth },
	{ path: '/login', 
		component: require('./views/Login.vue'),
		beforeEnter(to, from, next){
			if(auth.loggedIn()){
				next({
					path: to.query.redirect
				})
			}else {
				next();
			}
		}
	},
	{ path: '/logout',
		beforeEnter (to, from, next) {
			auth.logout();
			next('/');
		}
	},
	// Admin
	{ path: '/admin/dashboard', alias: '/admin', component: require('./views/Admin/Dashboard.vue'), beforeEnter: requireAdmin },
	{ path: '/admin/users', component: require('./views/Admin/Users.vue'), beforeEnter: requireAdmin },
	{ path: '/admin/users/create', component: require('./views/Admin/CreateUser.vue'), beforeEnter: requireAdmin },
	{ path: '/admin/users/:username/details', component: require('./views/Admin/UserDetails.vue'), beforeEnter: requireAdmin },
	{ path: '/admin/users/:username/assign', component: require('./views/Admin/UserAssign.vue'), beforeEnter: requireAdmin },
	{ path: '/admin/users/:username/edit', component: require('./views/Admin/UserEdit.vue'), beforeEnter: requireAdmin },
	{ path: '/admin/requests', component: require('./views/Admin/Requests.vue'), beforeEnter: requireAdmin },
	{ path: '/admin/records', component: require('./views/Admin/Records.vue'), beforeEnter: requireAdmin },
	{ path: '/admin/vehicles', component: require('./views/Admin/Vehicles.vue'), beforeEnter: requireAdmin },
	{ path: '/admin/vehicles/post', component: require('./views/Admin/PostVehicle.vue'), beforeEnter: requireAuth },
	{ path: '/admin/vehicles/:id/edit', component: require('./views/Admin/EditVehicle.vue'), beforeEnter: requireAdmin },
	{ path: '/admin/settings', component: require('./views/Admin/Settings.vue'), beforeEnter: requireAuth },
	{ path: '/admin/make-request', component: require('./views/Admin/MakeRequest.vue'), beforeEnter: requireAdmin },
	{ path: '/request-monitoring', component: require('./views/RequestMonitoring.vue'), beforeEnter: requireMonitoring },
	{ path: '/admin/make-request', component: require('./views/Request.vue'), beforeEnter: requireAdmin },
	{ path: '/records/request-monitoring', component: require('./views/ActualRequestMonitoring.vue'), beforeEnter: requireMonitoring },
	{ path: '/404', component: require('./views/404.vue') },
	{ path: '*', redirect: '/404' }
];

Vue.http.interceptors.push((request, next) => {
	NProgress.start();
	const authToken = auth.getToken();
    request.headers.set('Authorization', `Bearer ${authToken}`);

    next((response) => {
    	NProgress.done();
    	if (response.status == 404) {
    	  router.go('/');
    	} else if (response.status == 401 && response.data.refreshed_token) {
    	  auth.setToken(response.data.refreshed_token);
    	} else if (response.data.token_expired){
    		router.replace('/logout');
    	}
    });
});
let router = new VueRouter.default({
	routes,
	mode: 'history'
});

router.beforeEach((to, from, next) => {
	NProgress.start();
	next();
});

router.afterEach((to, from) => {
	sessionStorage.setItem('previousLink', from.fullPath);
	NProgress.done();
});

// auth.checkAuth();

module.exports.router = router;

let vue = new Vue({
	el: '#app',
	router,
	render: h => h(App)
});
