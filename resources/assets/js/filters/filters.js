module.exports = {
	pendingNotificationText(request){
		let text = '';
		let type = request.type.charAt(0).toUpperCase() + request.type.slice(1);
		switch(request.status){
			case 'sent':
				text = `${type} Gate Pass on ${request.date} has been sent`;
				break;
			case 'expired':
				text = `${type} Gate Pass has expired`;
				break;
			case 'declined':
				text = `${type} Gate Pass has been declined. [${request.reason}]`;
				break;
			default: 
				text = '';
				break;
		}
		return text;
	},
	approvedNotificationText(request){
		let text = '';
		let type = request.type.charAt(0).toUpperCase() + request.type.slice(1);
		switch(request.type){
			case 'vehicle':
				text = `${type} Gate Pass with vehicle number ${request.vehicle_number} is approved by ${request.approver} until ${request.until}`;
				break;
			default: 
				text = '';
				break;
		}
		return text;
	}
}