module.exports = {
	getDefaultTime(){
		return Vue.http.get(`${baseUrl}/api/requests/get-default-time`);
	},
	makeRequest(request){
		return Vue.http.post(`${baseUrl}/api/requests/create`, request);
	},
	makeAdminRequest(request){
		return Vue.http.post(`${baseUrl}/api/requests/create?admin=true`, request);
	},
	getNotificationsFor(username, params){
		return Vue.http.get(`${baseUrl}/api/${username}/notifications`, { params: params })
						.then((response) => {
							return response;
						}, (response) => {
							requestFailedBox(() => {
								this.getNotificationsFor(username, params);
							});
						});
	},
	clearNotificationsOf(notifications, username){
		return Vue.http.get(`${baseUrl}/api/${username}/notifications/clear`, { params: { 'notifications': notifications } })
						.then((response) => {
							Notification({
								title: 'Success!',
								message: response.data.message,
								type: 'success'
							});
							return response;
						}, (response) => {
							requestFailedBox(() => {
								this.clearNotificationsOf(notifications, username);
							});
						});
	},
	getRequestInfo(id){
		return Vue.http.get(`${baseUrl}/api/notifications/${id}/get-info`)
					.then((response) => {
						return response;
					}, (response) => {
						requestFailedBox(() => {
							this.getRequestInfo(id);
						});
					});
	},
	endorse(requestId, request){
		return Vue.http.post(`${baseUrl}/api/notifications/${requestId}/endorse`, request)
						.then((response) => {
							Notification({
								title: 'Success!',
								message: response.data.message,
								type: 'success'
							});
							return response;
						}, (response) => {
							if(response.status == 500 && response.data.error){
								displayErrors(response.data.error);
								return false;
							}
							requestFailedBox(() => {
								this.endorse(requestId, request);
							});
						});
	},
	bulkEndorse(requests, endorser){
		requests = _.map(requests, 'id');
		return Vue.http.post(`${baseUrl}/api/endorser/${endorser}/bulk-endorse`, requests)
						.then((response) => {
							Notification({
								title: 'Success!',
								message: response.data.message,
								type: 'success'
							});
							return response;
						}, (response) => {
							requestFailedBox(() => {
								this.bulkEndorse(requests, endorser);
							});
						});
	},
	bulkApprove(requests, approver){
		requests = _.map(requests, 'id');
		return Vue.http.post(`${baseUrl}/api/approver/${approver}/bulk-approve`, requests)
						.then((response) => {
							Notification({
								title: 'Success!',
								message: response.data.message,
								type: 'success'
							});
							return response;
						}, (response) => {
							requestFailedBox(() => {
								this.bulkApprove(requests, approver);
							});
						});
	},
	bulkDecline(requests, decliner){
		requests = _.map(requests, 'id');
		return Vue.http.post(`${baseUrl}/api/decline/${decliner}/bulk-decline`, requests)
						.then((response) => {
							Notification({
								title: 'Success!',
								message: response.data.message,
								type: 'success'
							});
							return response;
						}, (response) => {
							requestFailedBox(() => {
								this.bulkDecline(requests, decliner);
							});
						});
	},
	approve(requestId, request){
		return Vue.http.post(`${baseUrl}/api/notifications/${requestId}/approve`, request)
						.then((response) => {
							Notification({
								title: 'Success!',
								message: response.data.message,
								type: 'success'
							});
							return response;
						}, (response) => {
							if(response.status == 500 && response.data.error){
								displayErrors(response.data.error);
								return false;
							}
							requestFailedBox(() => {
								this.approve(requestId, request);
							});
						});
	},
	decline(requestId, request){
		return Vue.http.post(`${baseUrl}/api/notifications/${requestId}/decline`, request)
						.then((response) => {
							Notification({
								title: 'Success!',
								message: response.data.message,
								type: 'success'
							});
							return response;
						}, (response) => {
							if(response.status == 500 && response.data.error){
								displayErrors(response.data.error);
								return false;
							}
							requestFailedBox(() => {
								this.decline(requestId, request);
							});
						});
	},
	removeNotification(notificationId, username){
		return Vue.http.get(`${baseUrl}/api/${username}/notifications/${notificationId}/remove`)
						.then((response) => {
							Notification({
								title: 'Success!',
								message: response.data.message,
								type: 'success'
							});
							return response;
						}, (response) => {
							requestFailedBox(() => {
								this.removeNotification(notificationId, username);
							});
						});
	},
	getRequests(params, id, context){
		return Vue.http.get(`${baseUrl}/api/${id}/requests/for-table`, { 'params': params })
				.then((response) => {
					if(response.status == 200){
						context.requests = response.data;
					}
					return response;
				}, (response) => {
					requestFailedBox(() => {
						this.getRequests(params, id, context);
					});
				});
	},
	getTypedRequests(type, params, id, context){
		return Vue.http.get(`${baseUrl}/api/${id}/${type}/requests/for-table`, { 'params': params })
				.then((response) => {
					if(response.status == 200){
						context.requests = response.data;
					}
					return response;
				}, (response) => {
					requestFailedBox(() => {
						this.getTypedRequests(type, params, id, context);
					});
				});
	},
	validateVehicleUsage(vehicle){
		return Vue.http.get(`${baseUrl}/api/requests/validate-vehicle-usage`, { params: { vehicle } });
	}
};