let app = require('./../app');
let router = app.router;
module.exports = {
	user: {
		authenticated: false,
		id: 0,
		username: null,
		name: {
			first: null,
			last: null
		},
		email: null,
		role: 'normal',
		hasEndorsedUsers: false,
		hasApprovalUsers: false
	},
	login(context, credentials){
		Vue.http.post(`${baseUrl}/api/login`, credentials)
			.then((response) => {
				let userdata = response.data.userdata;
				this.setToken(response.data.token);
				this.user = {
					authenticated: true,
					id: userdata.id,
					username: userdata.username,
					name: {
						first: userdata.first_name,
						last: userdata.last_name
					},
					email: userdata.email,
					role: userdata.role,
					hasEndorsedUsers: userdata.hasEndorsedUsers,
					hasApprovalUsers: userdata.hasApprovalUsers
				};
				localStorage.setItem('user.role', this.user.role);
				if(this.user.role == 'admin'){
					window.location.href = '/admin/dashboard';
					return true;
				}else if(this.user.role == 'monitoring'){
					window.location.href = '/request-monitoring';
					return true;
				}else {
					window.location.href = '/';
					return true;
				}
			}, (response) => {
				displayErrors(response.data.error);
			});
	},
	getUserInfo(token, successCallback = null){
		token = token || localStorage.getItem('jwt-token');
		if (!token) {
		  return false;
		}
		Vue.http.get(`${baseUrl}/api/me`)
			.then((response) => {
				let data = response.data;
				this.user = {
					authenticated: true,
					id: data.id,
					username: data.username,
					name: {
						first: data.first_name,
						last: data.last_name
					},
					email: data.email,
					role: data.role,
					hasEndorsedUsers: data.hasEndorsedUsers,
					hasApprovalUsers: data.hasApprovalUsers
				};
				if(successCallback){
					successCallback(response);
				}
			}, (error) => {
				let data = error.data;
				if(!data.refreshed_token && error.status == 401){
					app.router.replace('/logout');
					return false;
				}
				MessageBox.alert('An error occured while connecting to the server.', 'Something went wrong!', {
					confirmButtonText: 'Try again',
					type: 'error',
					callback: action => {
						this.getUserInfo(token, successCallback);
					}
				});
			});
	},
	getToken(){
		return localStorage.getItem('jwt-token');
	},
	checkAuth(){
		this.getUserInfo();
	},
	setToken(token){
		localStorage.setItem('jwt-token', token);
		return this.getUserInfo(token);
	},
	loggedIn(){
		return this.getToken();
	},
	userRole(){
		return localStorage.getItem('user.role');
	},
	logout(){
		const token = this.getToken();

		if (this.loggedIn()) {
			this.user = {
				authenticated: false,
				id: 0,
				username: null,
				name: {
					first: null,
					last: null
				},
				email: null,
				role: 'normal'
			};
			localStorage.removeItem('jwt-token');
			localStorage.removeItem('user.role');
			window.location.href = '/login';
		}
	},
	changePassword(passwords, context){
		if(!passwords) return false;

		let fields = {
			user_id: this.user.id,
			username: this.user.username,
			current_password: passwords.current,
			new_password: passwords.new,
			new_password_confirmation: passwords.re_new
		};
		Vue.http.post(`${baseUrl}/api/change-password`, fields)
			.then((response) => {
				if(response.status == 200){
					context.$notify({
						title: 'Success!',
						message: response.data.message,
						type: 'success'
					});
					app.router.replace('/notifications');
				}
			}, (response) => {
				displayErrors(response.data.error);
			});
	}
}