let app = require('./../../app');

module.exports = {
    saveSettings(settingsdata, context, assign = false){
    	let form = new FormData();
    	form.append('default_duration', settingsdata.duration);
    	form.append('admin_email', settingsdata.admin_email);
    	form.append('image', settingsdata.logo);

		Vue.http.post(`${baseUrl}/api/admin/settings`, form)
			.then((response) => {
				context.settings.site_logo = baseUrl + response.data.site_logo;
				context.settings.duration = parseInt(response.data.default_duration);
				context.settings.admin_email = (response.data.admin_email);
				context.$root.$children[0].$emit('logoChanged', context.settings.site_logo);
			    context.$message({
			    	message: 'Settings has been saved successfully!',
			    	type: 'success'
			    });
			}, (response) => {
				displayErrors(response.data.error);
			});
    },
    getSettings(context){
		Vue.http.get(`${baseUrl}/api/admin/settings/`)
			.then((response) => {
				context.settings = response.data;
				context.settings.site_logo = baseUrl + response.data.site_logo;
				context.settings.duration = parseInt(response.data.default_duration);
				context.settings.admin_email = (response.data.admin_email);
			}, (response) => {
				displayErrors(response.data.error);
			});
    }
};