let app = require('./../../app');

module.exports =  {

	getActualRecords(params, context){
		return Vue.http.get(`${baseUrl}/api/requests-monitoring/actual-records`, { 'params': params })
				.then((response) => {
					if(response.status == 200){
						context.requests = _.values(response.data.requests);
						context.total = response.data.total;
					}
					return response;					
				});
	}
	
}