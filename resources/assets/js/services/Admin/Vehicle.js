let app = require('./../../app');

module.exports = {

    postVehicle(vehicledata, context, assign = false){

		Vue.http.post(`${baseUrl}/api/admin/vehicles`, vehicledata)
			.then((response) => {
				context.$notify({
					title: 'Success!',
					message: response.data.message,
					type: 'success'
				});
				context.$router.replace('/admin/vehicles');
			}, (response) => {
				displayErrors(response.data.error);
			});
    },

	getVehicles(params, context){

		Vue.http.get(`${baseUrl}/api/admin/vehicles`, { 'params': params })
				.then((response) => {
					
					if(response.status == 200){
						let data = [];
						_.map(response.data.data, function(value) {
                            data.push(value);
						});

						context.vehicles = data;
						context.vehiclesTotal = response.data.total;
					}
				});
	},

	getVehicle(id, context){

		Vue.http.get(`${baseUrl}/api/admin/vehicles/${id}/details`)
				.then((response) => {
					context.vehicledata = response.data;
					console.log(context.vehicledata);
				});
	},

	deleteVehicle(id, params, context){


		Vue.http.get(`${baseUrl}/api/admin/vehicles/${id}/delete`)
				.then((response) => {
					if(response.data == "Success"){
						context.$notify({
							title: 'Success!',
							message: 'You have a successfully deleted the record.',
							type: 'success'
						});
						this.getVehicles(params, context);
					}
				});
	}

};