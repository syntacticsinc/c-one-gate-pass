let app = require('./../../app');
let auth = require('./../Auth');

module.exports = {
	getAssignedUsersFor(username, context){
		if(!username) return false;

		Vue.http.get(`${baseUrl}/api/admin/users/${username}/assigned-users`)
				.then((response) => {
					context.selectedEndorsers = response.data.endorsers;
					context.selectedApprovers = response.data.approvers;
					context.selectedUsers = _.concat(response.data.endorsers, response.data.approvers);
					context.user = response.data.user;
				}, (response) => {
					requestFailedBox(() => {
						this.getAssignedUsersFor(username, context);
					});
				});
	},
	manageUser(userdata, context, assign = false, edit = false){
		if(!userdata) return false;
		let url = `${baseUrl}/api/admin/users/store`;
		if(edit){
			url = `${baseUrl}/api/admin/users/${context.username}/update`
		}
		if(!edit){
			userdata.endorsers = _.map(userdata.endorsers, 'id');
			userdata.approvers = _.map(userdata.approvers, 'id');
		}
		Vue.http.post(url, userdata)
			.then((response) => {
				context.$notify({
					title: 'Success!',
					message: response.data.message,
					type: 'success'
				});
				context.userdata = {
					username: null,
					first_name: null,
					last_name: null,
					email: null,
					role: '',
					department: '',
					password: null,
					password_confirmation: null
				};
				if(assign){
					context.$router.replace(`/admin/users/${userdata.username}/assign`);
					return false;
				}
				context.$router.replace(`/admin/users`);
			}, (response) => {
				if(response.status == 401){
					displayErrors(response.data.error);
					return true;
				}
				requestFailedBox(() => {
					this.manageUser(userdata, context, assign, edit);
				});
			});
	},
	searchFor(keyword, selectedUsers, username){
		return Vue.http.get(`${baseUrl}/api/admin/users/search/${keyword}`, { params: { 'selected_users': selectedUsers, 'username': username } })
					.then((response) => {
						return response;
					}, (response) => {
						requestFailedBox(() => {
							this.searchFor(keyword, selectedUsers, username);
						});
					});
	},
	searchAssociatedUsers(keyword, context){
		Vue.http.get(`${baseUrl}/api/admin/users/search/${keyword}?assoc=true&username=` + auth.user.username)
			.then((response) => {
				context.searchedUsers = response.data;
			}, (response) => {
				requestFailedBox(() => {
					this.searchAssociatedUsers(keyword, context);
				});
			});
	},
	searchAllUsers(keyword, context){
		Vue.http.get(`${baseUrl}/api/admin/users/search/${keyword}?admin=true`)
			.then((response) => {
				context.searchedUsers = response.data;
			}, (response) => {
				requestFailedBox(() => {
					this.searchAllUsers(keyword, context);
				});
			});
	},
	assignFor(endorsers, approvers, username, context){
		endorsers = _.map(endorsers, 'id');
		approvers = _.map(approvers, 'id');
		let data = {
			endorsers: endorsers,
			approvers: approvers,
			username: username
		};
		Vue.http.post(`${baseUrl}/api/admin/users/assign`, data)
			.then((response) => {
				if(response.status == 200){
					Notification({
					          title: 'Success!',
					          message: response.data.message,
					          type: 'success'
					        });
					context.$router.replace('/admin/users');
				}
			}, (response) => {
				if(response.status == 401){
					displayErrors(response.data.error);
					return true;
				}
				requestFailedBox(() => {
					this.assignFor(endorsers, approvers, username, context);
				});
			})
	},
	getUsers(params, context){
		return Vue.http.get(`${baseUrl}/api/admin/users/for-table`, { 'params': params })
				.then((response) => {
					if(response.status == 200){
						context.users = response.data;
					}
					return response;
				}, (response) => {
					requestFailedBox(() => {
						this.getUsers(params, context);
					});
				});
	},
	getUserByUsername(username, context){
		if(!username) context.$router.replace('/admin/users');
		Vue.http.get(`${baseUrl}/api/admin/users/${username}/details`)
				.then((response) => {
					context.user = response.data;
				}, (response) => {
					requestFailedBox(() => {
						this.getUserByUsername(username, context);
					});
				});
	},
	getUserData(username, context){
		if(!username) context.$router.replace('/admin/users');
		Vue.http.get(`${baseUrl}/api/admin/users/${username}/get-data`)
				.then((response) => {
					context.userdata = response.data;
				}, (response) => {
					requestFailedBox(() => {
						this.getUserData(username, context);
					});
				});
	},
	deleteUsers(users, context){
		users = _.map(users, 'id');
		return Vue.http.post(`${baseUrl}/api/admin/users/delete`, users)
				.then((response) => {
					Notification({
						title: 'Success!',
						message: response.data.message,
						type: 'success'
			        });
				}, (response) => {
					requestFailedBox(() => {
						this.deleteUsers(users, context);
					});
				});
	},
	resetUsers(users, context){
		users = _.map(users, 'id');
		return Vue.http.post(`${baseUrl}/api/admin/users/reset`, users)
				.then((response) => {
					Notification({
						title: 'Success!',
						message: response.data.message,
						type: 'success'
			        });
				}, (response) => {
					requestFailedBox(() => {
						this.resetUsers(users, context);
					});
				});
	},
	getFieldOptions(context){
		context.loading = true;
		return Vue.http.get(`${baseUrl}/api/admin/users/get-field-options`)
				.then((response) => {
					context.roles = response.data.roles;
					if(context.username){
						this.getUserData(context.username, context);
					}
					context.loading = false;
					return response;
				}, (response) => {
					requestFailedBox(() => {
						this.getFieldOptions(context);
					});
				});
	}
};