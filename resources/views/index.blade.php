<!DOCTYPE html>
<html lang="en">
    <head>
        {{-- <meta http-equiv="Content-Type" content="#"> --}}
        <!-- Chrome, Firefox OS, Opera and Vivaldi -->
        <meta name="theme-color" content="#F5F5F5">
        <link rel="icon" sizes="192x192" href="#">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#F5F5F5">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <meta name="apple-mobile-web-app-title" content="">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="apple-touch-icon-precomposed" href="#">
        <link rel='shortcut icon' href="{{ url('/favicon.ico') }}" type='image/x-icon'/ >
        <title>C-One Gate Pass</title>
        <!--Styles-->
        <!-- <link href='https://fonts.googleapis.com/css?family=Abril+Fatface' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'> -->
        <!-- Latest compiled and minified CSS -->
        {{-- <link rel="stylesheet" type="text/css" href="{{ url('/css/jquery-ui.min.css') }}"> --}}
        <link rel="stylesheet" href="{{ url('/css/font-awesome.min.css') }}" type="text/css" media="screen">
        <link rel="stylesheet" href="{{ url('/css/app.css') }}" type="text/css" media="screen">
        <!--Script-->
        <script type="text/javascript">
            window.Laravel = {
                csrfToken: '{{ csrf_token() }}'
            };
            window.baseUrl = "{{ url('/') }}";
            document.documentElement.className = 'js';
        </script>
    </head>
    <body>
        <div id="app"></div>
        <script type="text/javascript" src="{{ url('js/app.js') }}"></script>
    </body>
</html>
